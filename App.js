/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Provider} from "react-redux";
import { MenuProvider } from 'react-native-popup-menu';
import {Platform, StyleSheet, Text, View} from 'react-native';
import store from '@utils/managers/ReduxStoreManager/ReduxStoreManager';

import {SharedDrawerProvider} from "./src/providers/SharedDrawerContext";
import {RootScreen} from "./src/screens/RootScreen/RootScreen";

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
        <Provider store={store}>
            <SharedDrawerProvider>
                <MenuProvider>
                    <RootScreen/>
                </MenuProvider>
            </SharedDrawerProvider>
        </Provider>
    );
  }
}
// $FlowFixMe
console.disableYellowBox = true;
