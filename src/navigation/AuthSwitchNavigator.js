import { createSwitchNavigator } from "react-navigation";
import {SplashScreen} from "../screens/SplashScreen/SplashScreen";
import {LoginSettingsStackNavigator} from "./LoginSettingsStackNavigator";
import {MainStackNavigator} from "./MainStackNavigator";

export const AuthSwitchNavigator = createSwitchNavigator({
    SplashScreen: {screen: SplashScreen},
    Auth: {screen: LoginSettingsStackNavigator},
    App: {screen: MainStackNavigator}
}, {initialRouteName: 'SplashScreen'});
