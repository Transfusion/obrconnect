/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 12:05 PM
 */

import {createStackNavigator, createSwitchNavigator} from "react-navigation";
import {LoginScreen} from "../screens/LoginScreen/LoginScreen";


export const LoginSettingsStackNavigator = createStackNavigator({
    LoginScreen: {screen: LoginScreen}
}, {initialRouteName: 'LoginScreen'});
