/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 12:02 PM
 */

import {createStackNavigator} from "react-navigation";
import {HomeScreen} from "../screens/HomeScreen/HomeScreen";
import {ReceiveReferralsScreen} from "../screens/ReceiveReferralsScreen/ReceiveReferralsScreen";
import {CaseDetailsScreen} from "../screens/CaseDetailsScreen/CaseDetailsScreen";
import {MyWorklistScrollableTabNavigator} from "./MyWorklistScrollableTabNavigator";
import {MyProfileScreen} from "../screens/MyProfileScreen/MyProfileScreen";
import {CaseDetailsNStatusScreen} from "../screens/CaseDetailsNStatusScreen/CaseDetailsNStatusScreen";
import {HistoryScreen} from "../screens/HistoryScreen/HistoryScreen";
import {SettingsScreen} from "../screens/SettingsScreen/SettingsScreen";
import {HistoryCaseDetailsScreen} from "../screens/HistoryCaseDetailsScreen/HistoryCaseDetailsScreen";
import {MakeReferralScreen} from "../screens/MakeReferralScreen/MakeReferralScreen";
import {SampleChartsDemo} from "../screens/SampleChartsDemo/SampleChartsDemo";


export const MainStackNavigator = createStackNavigator({
    HomeScreen: {screen: HomeScreen},
    ReceiveReferralsScreen: {screen: ReceiveReferralsScreen},
    CaseDetailsScreen: {screen: CaseDetailsScreen},
    HistoryScreen: {screen: HistoryScreen},

    HistoryCaseDetailsScreen: {screen: HistoryCaseDetailsScreen},

    MakeReferralScreen: {screen: MakeReferralScreen},

    SettingsScreen: {screen: SettingsScreen},

    MyProfileScreen: {screen: MyProfileScreen},

    MyWorklistScrollableTabNavigator: {screen: MyWorklistScrollableTabNavigator},

    CaseDetailsNStatusScreen: {screen: CaseDetailsNStatusScreen},

    SampleChartsDemo: {screen: SampleChartsDemo}
}, {initialRouteName: 'HomeScreen'});
