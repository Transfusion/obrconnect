import {createMaterialTopTabNavigator} from "react-navigation";
import {Config} from "@utils/config/config";
import {SharedDrawerConsumer} from "../providers/SharedDrawerContext";
import Icon from "@assets/tmlmIcons";
import {LeadsReferredScreen} from "../screens/LeadsReferredScreen/LeadsReferredScreen";
import {LeadsReceivedScreen} from "../screens/LeadsReceivedScreen/LeadsReceivedScreen";

import { HeaderBackButton } from 'react-navigation';

import React, {Component} from "react";

export const MyWorklistScrollableTabNavigator = createMaterialTopTabNavigator({
    LeadsReferredScreen: {screen: LeadsReferredScreen},
    LeadsReceivedScreen: {screen: LeadsReceivedScreen}
}, {
    tabBarOptions: {
        upperCaseLabel: false,
            // scrollEnabled: true,
            activeTintColor: Config.accentColor,
        inactiveTintColor: '#fff',
        inactiveBackgroundColor: Config.primaryColor,
        activeBackgroundColor: Config.primaryColor,

        labelStyle: {
            fontSize: 15,
        },

        showIcon: false,
            style: {
            paddingTop: 0,
                paddingBottom: 0,
                height: 60,
                // height: 60,
                backgroundColor: Config.primaryColor,
                borderBottomWidth: 0,
                shadowColor: Config.accentColor,
                elevation: 0,
            // maxHeight: 50,
        },
        indicatorStyle: {
            // height: 0,
            backgroundColor: Config.accentColor
        }
    },
    initialRouteName: 'LeadsReferredScreen'
});

MyWorklistScrollableTabNavigator.navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};
    /*const activeRoute = navigation.state.routes[navigation.state.index];
    const routeName = _lookupRouteName(activeRoute.routeName);*/

    // You can do whatever you like here to pick the title based on the route name
    return {
        gesturesEnabled: true,
        title: 'My Worklist',
        headerStyle: {
            backgroundColor: Config.primaryColor,
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'normal',
            fontSize: 18,
            fontFamily: Config.OBRFont.semiBold,
        },
        headerLeft:(<HeaderBackButton title={'Back'} tintColor={'#fff'} onPress={()=>{navigation.navigate('HomeScreen')}}/>),
        headerRight: (
            <SharedDrawerConsumer>
                {({ drawerOpened }) => (
                    <Icon
                        // onPress={() => {ManageProfileNavigator.eventEmitter.emit('hamburger_pressed')}}
                        onPress = {() => {
                            drawerOpened(true);
                        }}
                        style={{paddingRight: 20}} name='icon_menu' color={'#fff'} />
                )}

            </SharedDrawerConsumer>
        ),
    };
};
