
/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 10:53 AM
 */

import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {RightDrawer} from "@components/RightDrawer";


const SharedDrawerContext = React.createContext();

export class SharedDrawerProvider extends Component {

    constructor(props){
        super(props);
        this.state = {
            drawerOpen: false
        }
    }

    _drawerOpened(opened: boolean) {
        this.setState({
            drawerOpen: opened
        });
    }

    render() {
        const {children} = this.props;

        return (
            <SharedDrawerContext.Provider
                value={{
                    drawerOpened: this._drawerOpened.bind(this),
                    drawerIsOpen: this.state.drawerOpen
                }}>

                <RightDrawer>
                    {children}
                </RightDrawer>

            </SharedDrawerContext.Provider>
        )
    }
}

export const SharedDrawerConsumer = SharedDrawerContext.Consumer;
