/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 1:05 PM
 */

import React, {Component} from "react";
import {TouchableOpacity, Text} from "react-native";
import {Config} from "@utils/config/config";

/** The yellow/blue Save/Submit Feedback/etc. button. **/

export class OBRButton extends Component {

    constructor(props){
        super(props);
    }

    render() {
        const {text} = this.props;
        const {style} = this.props;
        const {onPress} = this.props;
        const {disabled} = this.props;

        return (
            <TouchableOpacity
                // muted={disabled}
                // disabled={disabled}
                // styleName="primary"
                onPress={onPress}
                style={{fontFamily: Config.OBRFont.bold, backgroundColor: disabled ? Config.mutedColor : Config.accentColor,
                    borderRadius: 5, borderWidth: 0, alignItems: 'center', justifyContent: 'center', padding: 10,
                    ...style}}>
                <Text style={{fontSize: 15, color: disabled ? '#000' : Config.primaryColor, fontFamily: Config.OBRFont.bold}}>{text}</Text>
            </TouchableOpacity>
        )
    }
}
