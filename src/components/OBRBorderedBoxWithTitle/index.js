import React, {Component} from "react";
import {View} from 'react-native';
import {Text} from '@shoutem/ui';
import styles from './styles';


export class OBRBorderedBoxWithTitle extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        const {children, title, style, titleStyle} = this.props;

        return (
            <View style={[style, styles.box]}>
                <Text style={{backgroundColor: '#fff', paddingRight: 10,
                    paddingLeft: 10, position: 'absolute', top: -10, ...titleStyle}}>{title}</Text>
                {children}
            </View>
        );
    }

}
