import {StyleSheet} from 'react-native';
import {Config} from "@utils/config/config";


const styles = StyleSheet.create({
    box: {
        marginTop: 10,
        padding: 10,
        paddingTop: 20,
        borderColor: Config.accentColor,
        borderWidth: 1,
        /*borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5*/
    }

});

export default styles;