/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 2:14 PM
 */

import {Component} from "react";

import React from "react";
import {TouchableOpacity} from "@shoutem/ui";
import styles from './styles';
import {Text} from "@shoutem/ui";
import {Config} from "@utils/config/config";
import {View} from "react-native";

export class HomeScreenBtn extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        // const {name} = this.props;
        const {icon} = this.props;
        // const {imageColor} = this.props;
        const {text} = this.props;
        const {onPress} = this.props;
        // const {buttonHeight} = this.props;

        const {style} = this.props;

        return (
            <TouchableOpacity
                onPress={onPress}
                style={{...styles.homeScreenBtn, ...style}}>
                {icon}
                {this.props.badgeText ? <View style={{justifyContent: 'center', position: 'absolute', right: 5, top: 5, alignItems: 'center',
                    borderRadius: 50, padding: 5, marginLeft: 15, paddingRight: 10, paddingLeft: 10,
                    backgroundColor: '#FC0D1B'}}>
                    <Text style={{color: '#fff', fontSize: 15, fontFamily: Config.OBRFont.bold}}>{this.props.badgeText}</Text>
                </View> : null }
                <Text style={{textAlign: 'center', color: '#444', paddingTop: 5, fontFamily: Config.OBRFont.semiBold}}>{text}</Text>
            </TouchableOpacity>
        )
    }
}

