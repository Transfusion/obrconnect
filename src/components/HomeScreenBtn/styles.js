import {StyleSheet} from "react-native";
import {Config} from "@utils/config/config";


export default StyleSheet.create({
    homeScreenBtn: {
        // backgroundColor: '#aaa',
        // height: 50,
        borderRadius: 10,
        margin: Config.homeScreenIconGap,

        padding: 10,
        textAlign: 'center',
        
        shadowColor: 'rgba(0,0,0, .4)', // IOS
        shadowOffset: { height: 1, width: 1 }, // IOS
        shadowOpacity: 1, // IOS
        shadowRadius: 1, //IOS
        backgroundColor: '#fff',
        elevation: 2, // Android,

        flexDirection: 'column',
        flexGrow: 1,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});