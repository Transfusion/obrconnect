
/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 1:36 PM
 */

import React, {Component} from "react";
import {Text, View, TouchableOpacity} from 'react-native';

import Icon from "@assets/tmlmIcons";
import {Config} from "@utils/config/config";

export class RightDrawerItemRow extends Component {
    constructor(props){
        super(props);
    }

    render() {
        const {onPress, tmlmIconName, text, badgeText} = this.props;

        return (
            <TouchableOpacity onPress={onPress} style={{flexDirection: 'row', padding: 20, alignItems: 'center'}}>
                <Icon style={{paddingRight: 15}} name={tmlmIconName} color={'#fff'} size={20} />
                <Text style={{color: '#fff', fontFamily: Config.OBRFont.bold}}>{text}</Text>

                {badgeText ?
                    <View style={{justifyContent: 'center', alignItems: 'center',
                        borderRadius: 50, padding: 5, marginLeft: 15, paddingRight: 10, paddingLeft: 10,
                        backgroundColor: '#FC0D1B'}}>
                        <Text style={{color: '#fff', fontSize: 15, fontFamily: Config.OBRFont.bold}}>{badgeText}</Text>
                    </View>:
                    null}
            </TouchableOpacity>
        )
    }
}
