/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 12:52 PM
 */

import React, {Component} from "react";
import {Dimensions, View, SafeAreaView, ImageBackground} from "react-native";
import Drawer from "react-native-drawer";

import {SharedDrawerConsumer} from "../../providers/SharedDrawerContext";

import images from '@assets/images';
import {RightDrawerItemRow} from "./RightDrawerItemRow";
import NavigationService from "@utils/navigation/NavigationService";
import {RightDrawerHeader} from "./RightDrawerHeader";
import {_uatMode, Config} from "@utils/config/config";
import {login, logout} from "@reducers/authReducer";
import {connect} from "react-redux";

/** https://reactnavigation.org/docs/en/navigating-without-navigation-prop.html **/
class _RightDrawer extends Component {

    constructor(props){
        super(props);
        this.state = {
            screenDims: Dimensions.get("window")
        }
    }

    _dimensionsChangeHandler(dims) {
        this.setState({screenDims: dims.screen});
    }

    componentWillMount() {
        Dimensions.addEventListener("change", this._dimensionsChangeHandler.bind(this));
    }

    componentDidMount() {

    }

    componentWillUnmount() {
        // Important to stop updating state after unmount
        Dimensions.removeEventListener("change", this._dimensionsChangeHandler.bind(this));
    }

    render(){
        const {children} = this.props;

        const {width, height} = this.state.screenDims;

        let drawerContent =
            <SharedDrawerConsumer>
                {({ drawerOpened }) => (
                    <View>
                        <ImageBackground
                            styleName="large-wide"
                            resizeMode={'cover'}
                            style={{flexDirection: 'row', justifyContent: 'flex-end', height: height * 0.3}}
                            source={images.userBg}>
                            <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.5)'}}>
                                <RightDrawerHeader/>
                            </View>

                        </ImageBackground>

                        <View>
                            <RightDrawerItemRow text={'My Profile'} tmlmIconName={'icon_myprofile'}
                                                onPress={() => {
                                                    NavigationService.navigate('MyProfileScreen');
                                                    drawerOpened(false);
                                                }}/>

                            {/*TODO: Fetch number of pending notifications from the API*/}
                            {_uatMode ?
                                <RightDrawerItemRow text={'Notifications'} tmlmIconName={'icon_notification'}
                                                    badgeText={'6'}
                                                    onPress={() => {
                                                        NavigationService.navigate('NotificationsOverviewScreen');
                                                        drawerOpened(false);
                                                    }}/>
                                                    : null}


                            {/*<RightDrawerItemRow text={'Contact us'} tmlmIconName={'icon_phone'}
                                                onPress={() => {
                                                    NavigationService.navigate('ContactUsScreen');
                                                    drawerOpened(false);
                                                }}/>*/}

                            <RightDrawerItemRow text={'Settings'} tmlmIconName={'icon_tool'}
                                                onPress={async () => {
                                                    NavigationService.navigate('SettingsScreen');
                                                    drawerOpened(false);
                                                }}/>

                            <RightDrawerItemRow text={'Log Out'} tmlmIconName={'icon_logout'}
                                                onPress={async () => {
                                                    await this.props.logout();
                                                    NavigationService.navigate('LoginScreen');
                                                    drawerOpened(false);
                                                }}/>

                        </View>

                    </View>
                )}
            </SharedDrawerConsumer>;

        return (
            <SharedDrawerConsumer>
                { ({drawerOpened, drawerIsOpen}) => (
                    <Drawer
                        onClose={() => {drawerOpened(false)}}
                        tapToClose={true}
                        openDrawerOffset={width - 300}
                        side='right'
                        open={drawerIsOpen}
                        // ref={ref => this._drawer = ref}
                        content={drawerContent}
                        styles={{drawer: {backgroundColor: '#252525'}}}>

                        {children}
                    </Drawer>
                ) }

            </SharedDrawerConsumer>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
        tokenInfo: state.authReducer.tokenInfo,
        profileInfo: state.authReducer.profileInfo
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...ownProps,
        logout: () => {
            return dispatch(logout());
        }
    }
};


export const RightDrawer = connect(mapStateToProps, mapDispatchToProps)(_RightDrawer);
