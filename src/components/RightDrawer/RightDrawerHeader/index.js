/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 1:32 PM
 */

import React, {Component} from "react";
import {Image, Text, View} from 'react-native';
import images from "@assets/images";

import styles from './styles';
import {connect} from "react-redux";
import {login, logout} from "../../../reducers/authReducer";
import moment from "moment";
import {Config} from "@utils/config/config";
// import {Title} from "@shoutem/ui";

class _RightDrawerHeader extends Component {

    constructor(props){
        super(props);
    }


    render() {
        // const {image, lastLogin} = this.props; Don't need this, we'll be reading profile info straight from the redux store
        // Because the drawer is technically present even before we login, just that it's not visible
        // If we are not logged in, profileInfo is guaranteed to be null.
        if (this.props.profileInfo) {
            let loginDt = Date.parse(this.props.lastLoginDt);
            let loginDtMoment = moment(loginDt);

            return (
                <View style={{
                    flexDirection: 'row',
                    flex: 1,
                    paddingLeft: 5,
                    flexGrow: 1,
                    justifyContent: 'space-around',
                    alignItems: 'center'
                }}>
                    {/*<Image style={{ resizeMode: 'cover', height: 100, width: 100}} source={images.user03} />*/}
                    <View style={{flexDirection: 'column'}}>
                        <Text style={{fontFamily: Config.OBRFont.bold,
                            fontSize: 25,
                            color: Config.accentColor}}>{this.props.profileInfo.name}</Text>
                        <Text style={styles.lastLogin}>
                            Last login was {loginDtMoment.format('lll')}
                        </Text>
                        <View style={{height: 10}} />
                        <Text style={styles.lastLogin}>
                            {/*Points: {this.props.profileInfo.Points}*/}
                        </Text>
                    </View>
                </View>
            )
        }
        else {
            return <View/>
        }
    }
}



const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
        // tokenInfo: state.authReducer.tokenInfo,
        profileInfo: state.authReducer.profileInfo,
        lastLoginDt: state.authReducer.lastLoginDt
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...ownProps,
    }
};

export const RightDrawerHeader = connect(mapStateToProps, mapDispatchToProps)(_RightDrawerHeader);
