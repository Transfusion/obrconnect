/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 1:10 PM
 */

import {Component} from "react";
import React from "react";
import {TextInput, Animated,View, Platform,Easing} from "react-native";
import { TextInputMask } from 'react-native-masked-text'

import {Config} from "@utils/config/config";
// import TextInputMask from "react-native-text-input-mask";

/** TextInput with OBR accented border and blue text **/

const AnimatedTextInput = Animated.createAnimatedComponent(TextInput);
const AnimatedMaskedTextInput = Animated.createAnimatedComponent(TextInputMask);

export class ThemedTextInput extends Component {
    constructor(props) {
        super(props);
        this.textInputBorderAnimatedValue = new Animated.Value(0);
    }

    componentDidMount() {
        // this._startFocusAnimation();
    }

    _startFocusAnimation() {
        Animated.timing(this.textInputBorderAnimatedValue, {
            toValue: 150,
            duration: 500
        }).start();
    }

    _startDefocusAnimation() {
        Animated.timing(this.textInputBorderAnimatedValue, {
            toValue: 0,
            duration: 500
        }).start();
    }

    render() {
        const {onChangeText, placeholder, value, mask} = this.props;

        const {multiline} = this.props;
        const {keyboardType, returnKeyType} = this.props;
        const {numberOfLines} = this.props;
        const {style} = this.props;
        const {wrapperStyle} = this.props;
        const {pointerEvents} = this.props;
        const {scrollEnabled} = this.props;
        const {onBlur} = this.props;
        const {disabled} = this.props;

        let {animatedTextInputRef} = this.props;
        if (!animatedTextInputRef) {
            animatedTextInputRef = ref => {}
        }
        let {secureTextEntry} = this.props;
        secureTextEntry = !!secureTextEntry;
        // const AnimatedTextInput = Animated.createAnimatedComponent(TextInput);

        const interpolateColor = this.textInputBorderAnimatedValue.interpolate({
            inputRange: [0, 150],
            outputRange: ['rgb(0,0,0)', Config.accentColor]
        });

        // const animatedStyle = { backgroundColor: interpolateColor};

        return (

            <View pointerEvents={pointerEvents} style={{...wrapperStyle}}>
                {!mask ? <AnimatedTextInput

                    /*ref={r => {
                        if ( r.getNode ) {
                            return internalTextInputRef(r.getNode());
                        }
                        else {
                            return internalTextInputRef(r);
                        }
                    }}*/
                    // Required by ESP
                    autoCorrect={false}
                    keyboardType={ (keyboardType || secureTextEntry) || Platform.OS !== 'android' ? keyboardType : 'visible-password'}
                    returnKeyType={returnKeyType}
                    secureTextEntry={secureTextEntry}
                    onBlur = {onBlur}
                    ref = {ref => animatedTextInputRef(ref)}
                    scrollEnabled={scrollEnabled}
                    disabled={disabled}
                    multiline={multiline}
                    numberOfLines={numberOfLines}
                    blurOnSubmit={false}
                    onChangeText={onChangeText}
                    placeholder={placeholder}
                    selectionColor={Config.primaryColor}
                    style={{...style, padding: 10, color: Config.primaryColor,borderRadius: 5, borderWidth: 0.5,
                        borderColor: interpolateColor, fontFamily: Config.OBRFont.regular}}
                    value={value}
                    onFocus={this._startFocusAnimation.bind(this)}
                    onEndEditing={this._startDefocusAnimation.bind(this)}

                /> : <AnimatedMaskedTextInput
                    value={value}
                    type={'custom'}
                    options={{
                        mask: mask
                    }}

                    autoCorrect={false}
                    keyboardType={(keyboardType || secureTextEntry) || Platform.OS !== 'android' ? keyboardType : 'visible-password'}
                    returnKeyType={returnKeyType}
                    secureTextEntry={secureTextEntry}
                    onBlur = {onBlur}
                    ref = {ref => animatedTextInputRef(ref)}
                    scrollEnabled={scrollEnabled}
                    multiline={multiline}
                    numberOfLines={numberOfLines}
                    onChangeText={onChangeText}
                    placeholder={placeholder}
                    selectionColor={Config.primaryColor}
                    style={{...style, padding: 10, color: Config.primaryColor,borderRadius: 5, borderWidth: 0.5,
                        borderColor: interpolateColor, fontFamily: Config.OBRFont.regular}}
                    onFocus={this._startFocusAnimation.bind(this)}
                    onEndEditing={this._startDefocusAnimation.bind(this)}
                />}
            </View>
        )
    }
}

//https://stackoverflow.com/questions/51526461/how-to-use-react-forwardref-in-a-class-based-component
// export const ThemedTextInput = React.forwardRef((props, ref) => <_ThemedTextInput innerRef={ref} {...props} />);
