import {StyleSheet} from 'react-native';

export const colors = {
    black: '#1a1917',
    white: '#ffffff',
    gray: '#888888',
    background1: '#B721FF',
    background2: '#21D4FD'
};

export default StyleSheet.create({
    paginationDot: {
        width: 10,
        height: 10,
        borderRadius: 10,
        marginHorizontal: 8
    }
})