/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 3:55 PM
 */

import Carousel, {Pagination} from 'react-native-snap-carousel';
import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {Dimensions} from "react-native";
import {SlideshowImage} from "./SlideshowImage";

import images from '@assets/images';
import styles, {colors} from './styles';

export class HomeSlideshow extends Component {

    constructor(props){
        super(props);
        this.state = {
            activeDotIndex: 0,
            primedIndex: 0
        }
    }

    _initiateCarouselAutoLoop() {
        this._autoLoop = setInterval(() => {
            let itemsInCarousel = this.getCarouselItems().length;
            // this.setState({activeDotIndex: (this.state.activeDotIndex + 1) % itemsInCarousel});
            this._carousel.snapToItem((this.state.activeDotIndex + 1) % itemsInCarousel);
        }, 5000);
    }

    _tearDownCarouselAutoLoop() {
        clearInterval(this._autoLoop);
    }

    componentWillMount() {
        this._initiateCarouselAutoLoop();
    }

    componentWillUnmount() {
        this._tearDownCarouselAutoLoop();
    }

    _renderItem ({item, index}) {
        // const {onMerchantPress} = this.props;
        return (
            <SlideshowImage primedIndexOfParentSlideshow={this.state.primedIndex}
                            key={index}
                            index={index}
                            text={item.text}
                            image={item.image}/>
        );
    }

    getCarouselItems() {
        return [
            /*{
                name: 'experience_e_claims_now',
                image: images.slider01,
                text: 'Experience e-Claims service now!',
                // text positioning info
                tweenDirection: SlideshowImage.tweenDirection.BOTTOM,
                textPlacement: SlideshowImage.textPlacement.LEFT
            },
            {
                name: 'life_insurance_e_claims',
                image: images.slider02,
                text: 'Life Insurance e-Claims',
                tweenDirection: SlideshowImage.tweenDirection.BOTTOM,
                textPlacement: SlideshowImage.textPlacement.LEFT
            }*/
            {
                name: 'uob_demo',
                image: images.obrSlide,
                text: '',
                // text positioning info
                tweenDirection: SlideshowImage.tweenDirection.BOTTOM,
                textPlacement: SlideshowImage.textPlacement.LEFT
            }
        ]

    }

    render () {
        // const {featuredMerchants} = this.props;

        const {style} = this.props;

        return (
            <View style={style}>
                <Carousel
                    // style={{position: 'absolute'}}
                    ref={(c) => { this._carousel = c; }}
                    data={this.getCarouselItems()}
                    renderItem={this._renderItem.bind(this)}
                    sliderWidth={Dimensions.get('window').width}
                    itemWidth={Dimensions.get('window').width}
                    onBeforeSnapToItem={(index) => this.setState({primedIndex: index}) }
                    onSnapToItem={(index) => this.setState({ activeDotIndex: index }) }
                />

                <View style={{position: 'absolute', left: 0, right: 0, bottom: 10, alignItems: 'center'}}>
                    <Pagination
                        dotsLength={this.getCarouselItems().length}
                        activeDotIndex={this.state.activeDotIndex}
                        // containerStyle={styles.paginationContainer}
                        dotColor={'rgba(255, 255, 255, 0.92)'}
                        dotStyle={styles.paginationDot}
                        inactiveDotColor={colors.white}
                        inactiveDotOpacity={0.4}
                        inactiveDotScale={0.6}
                        // carouselRef={this._slider1Ref}
                        // tappableDots={!!this._slider1Ref}
                    />
                </View>
            </View>
        );
    }

}
