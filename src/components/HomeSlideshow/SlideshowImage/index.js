/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 2:19 PM
 */

import {Component} from "react";
import React from "react";

import images from '@assets/images';
import {Overlay, Tile, Caption, TouchableOpacity, ImageBackground, Title} from "@shoutem/ui";
import {Animated, View} from "react-native";

export class SlideshowImage extends Component {

    // Each image may have some accompanying text which eases in
    static tweenDirection = {
        LEFT: 0,
        RIGHT: 1,
        BOTTOM: 2,
        TOP: 3,
    };

    static textPlacement = {
        LEFT: 0,
        RIGHT: 1
    };

    constructor(props) {
        super(props);
        this.state = {
            textTranslateX: new Animated.Value(0),
            textTranslateY: new Animated.Value(0),
        }
    }

    _textEaseInAnimation() {
        // Always ease in from left for now TODO: pass tweenDirection in as a prop
        // this.setState({textTranslateX: new Animated.Value(-1000)});
        this.state.textTranslateX.setValue(-500);
        Animated.timing(this.state.textTranslateX, {
            toValue: 0,
            duration: 1000
        }).start();
    }

    componentWillUpdate() {

    }

    render(){
        const {onPress} = this.props;
        const {image, text} = this.props;

        this._textEaseInAnimation();
        return <TouchableOpacity style={{flex:1, overflow: 'hidden', }} onPress={onPress}>
            <ImageBackground opacity={0.8}
                style={{flex:1}}

                // styleName="large-wide"
                source={image}
                resizeMode={'contain'}>

                <Overlay style={{backgroundColor: "rgba(255,255,255,0)"}} styleName="image-overlay">
                    {this.props.primedIndexOfParentSlideshow === this.props.index &&
                    <Animated.View style={{transform: [{translateX: this.state.textTranslateX}]}}>
                        <Title style={{padding: 10, fontWeight:"400", fontSize: 30, color: '#002b39'}}>{text}</Title>
                    </Animated.View> }
                </Overlay>

            </ImageBackground>

        </TouchableOpacity>
    }
}