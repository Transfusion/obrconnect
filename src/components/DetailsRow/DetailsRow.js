/*
 * Created and developed by Bryan Kok (bryan.wyern1@gmail.com)
 * Last modified 4/14/19 1:13 AM
 */

import React, {Component} from "react";
import {Text, View} from 'react-native';

import styles from './styles';
import {Config} from "../../utils/config/config";

export class DetailsRow extends Component {

    constructor(props){
        super(props);
    }

    render() {
        // In case we want to display more than just text!
        let {leftView, rightView} = this.props;
        return (
            <View style={styles.container}>
                {leftView ? leftView : <View style={{flex: 1}}>
                    <Text selectable style={{color: '#222', fontSize: 15}}>
                        {this.props.left}
                    </Text>
                </View>}

                <View style={{flex: 1}}>
                    {rightView ? rightView : <Text selectable style={{color: Config.primaryColor, fontSize: 15, textAlign: 'right'}}>
                        {this.props.right}
                    </Text>}
                </View>
            </View>
        )
    }
}
