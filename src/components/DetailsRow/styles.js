/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 9:44 PM
 */

import {StyleSheet} from "react-native";

export default StyleSheet.create({

    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        /*marginTop: 5,
        marginBottom: 10,*/
    },
});
