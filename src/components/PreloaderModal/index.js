import React, { Component } from 'react';
import {
    View,
    Modal,
    Platform,
    ActivityIndicator
} from 'react-native';

import styles from './styles';
import {Config} from "../../utils/config/config";

/** The loading model. **/
export class PreloaderModal extends Component {

    componentWillReceiveProps(nextProps) {
        if (!nextProps.loading && this.props.loading
            && Platform.OS === 'android' && this.props.onDismiss) {
            this.props.onDismiss();
        }
    }

    render() {
        const {loading} = this.props;
        const {onRequestClose} = this.props;
        const {onDismiss} = this.props;

        return (
            <Modal transparent={true}
                   onRequestClose={onRequestClose}
                   onDismiss={onDismiss}
                visible={loading}>
                <View style={styles.modalBackground}>
                    <ActivityIndicator size="large" color={Config.primaryColor} />
                </View>
            </Modal>
        )
    }

}
