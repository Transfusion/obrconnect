
export const DRAWER_ACTION = {
    SHOW_RIGHT_NAV_DRAWER: "SHOW_RIGHT_NAV_DRAWER",
};

/** actions begin here **/
export const showRightNavDrawer = isShown => {
    return (dispatch, getState) => {
        dispatch({type: DRAWER_ACTION.SHOW_RIGHT_NAV_DRAWER,
            payload: isShown});
    }
};

/** actions end here **/

const initialState = {
    rightNavDrawerIsOpen: false,
};


export const drawerReducer = (state = initialState, action) => {
    switch(action.type){
        case DRAWER_ACTION.SHOW_RIGHT_NAV_DRAWER:
            return {
                ...state,
                rightNavDrawerIsOpen: action.payload
            }
    }
    return state;
};