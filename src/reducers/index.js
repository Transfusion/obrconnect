/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 1:23 PM
 */

import {combineReducers} from "redux";
import {drawerReducer} from "./drawerReducer";
import {authReducer} from "./authReducer";


export default combineReducers({
    authReducer: authReducer,
    // drawerReducer: drawerReducer,
})