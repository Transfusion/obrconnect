
// import type {LoginInfo} from "@utils/POJOInterfaces";
import {getLoginInfo, performLogin, performLogout} from "@utils/managers/AuthManager/AuthManager";

import {AsyncStorageUtil} from "@utils/storage/AsyncStorageUtil";
import {AsyncStorageKeys} from "@utils/config/config";

// import {ReturnValues} from "@utils/api/BancaAPIService";

export const AUTH_ACTIONS = {
    LOAD_PROFILE: "LOAD_PROFILE",
    LOGIN: "LOGIN",
    LOGOUT: "LOGOUT",
};

/** actions begin here **/
export const checkLoggedIn = () => {
    return async (dispatch, getState) => {
        let loginInfo = await getLoginInfo();
        console.log('loginInfo from within reducer', loginInfo);
        if (loginInfo != null) {
            dispatch({type: AUTH_ACTIONS.LOAD_PROFILE, payload: loginInfo});
            return loginInfo;
        } else {
            // removing should be handled by getLoginInfo();
            // await AsyncStorageUtil.removeLocalStorage(AsyncStorageKeys.KEY_ONETOKIO_SESSION_TOKEN);
            return null;
        }
    }
};

export const login = (username: string, userId: string, password: string) => {
    return async (dispatch, getState) => {
        let loginResult = await performLogin(username, userId, password);
        // short-circuiting
        if (!!loginResult) {
            dispatch(checkLoggedIn());
        }
        return loginResult;
    }
};

/*export const updateUserId = (userId: string) => {
    return {
        type: AUTH_ACTIONS.UPDATE_USER_ID,
        payload: userId
    }
};*/

export const logout = () => {
    return async (dispatch, getState) => {
        let logoutResp = await performLogout();
        dispatch({type: AUTH_ACTIONS.LOGOUT});
        return !!logoutResp;
    }
};

const initialState = {
    // tokenInfo: null,
    profileInfo: null,

    // userId to be updated upon entering the splash screen
    // userId: null,
    lastLoginDt: null
};

export const authReducer = (state = initialState, action) => {
    // console.log('redux state', state);
    switch(action.type){
        case AUTH_ACTIONS.LOAD_PROFILE:
            // preserve the userId loaded in during the splash screen.
            return {...action.payload, ...{userId: state.userId}};
        case AUTH_ACTIONS.LOGOUT:
            // preserve userId upon logging out.
            return {...state, ...{tokenInfo: null, lastLoginDt: null, profileInfo: null}};
        /*case AUTH_ACTIONS.UPDATE_USER_ID:
            return {...state, userId: action.payload}*/
    }
    return state;
};
