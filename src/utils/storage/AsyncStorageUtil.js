import {AsyncStorage} from "react-native";


export class AsyncStorageUtil {
    static async setLocalStorage(key, data) {
        await AsyncStorage.setItem(key, JSON.stringify(data));
    }

    static async getLocalStorage(key) {
        const item = await AsyncStorage.getItem(key);
        return JSON.parse(item);
    }

    static async removeLocalStorage(key) {
        try {
            await AsyncStorage.removeItem(key);
            return true;
        }
        catch(exception) {
            return false;
        }
    }
}