
export class OBRAPIService {
    static async login(username: string, password: string) {
        return {
            token: 'sample_token_aabbcdd'
        }
    }

    static async getOwnProfile() {
        return {
            name: 'Laurence Lau',
            contact_no: '011-222-3333',
            member_since: new Date('2017/01/01'),
            staff_id: '22228888',
            branch: 'Branch A',
            business_unit: 'Unit A'
        }
    }

    static async logout() {
        return true;
    }
}
