/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 11:48 AM
 */

//@flow
import * as Keychain from 'react-native-keychain';

import {AsyncStorageUtil} from "@utils/storage/AsyncStorageUtil";
import {AsyncStorageKeys} from "@utils/config/config";

import {OBRAPIService} from "@utils/api/OBRAPIService";
import {isSameDay} from "@utils/utils";
import {Config} from "../../config/config";

export async function performLogin(username: string, userId: string, password: string) {
    await Keychain.resetInternetCredentials(Config.obrTokenKeychainServer);
    let loginResponse = await OBRAPIService.login(username, userId, password);
    if (!loginResponse){
        return null;
    }
    if (loginResponse) {
        await Keychain.setInternetCredentials(Config.obrTokenKeychainServer, username, JSON.stringify(loginResponse));
        await AsyncStorageUtil.setLocalStorage(AsyncStorageKeys.KEY_LAST_LOGIN_DT, new Date());
    }

    return loginResponse;
}

// Should return a falsy value if not present.
export async function getSessionToken() {
    // return await AsyncStorageUtil.getLocalStorage(AsyncStorageKeys.KEY_SELF_SERVICE_SESSION_TOKEN);
    let _creds = await Keychain.getInternetCredentials(Config.obrTokenKeychainServer);
    if (_creds) {
        const { username, password } = _creds;
        // console.log('token: ', password);
        return JSON.parse(password);
    }
    return null;
}

export async function performLogout() : Promise<?boolean> {
    let token = await getSessionToken();
    if (!token) {
        return null;
    }

    let successfullyLoggedOut = await OBRAPIService.logout(token);
    await Keychain.resetInternetCredentials(Config.obrTokenKeychainServer);
    return successfullyLoggedOut;
}

export async function getLoginInfo() {
    let tokenInfo = await getSessionToken();
    let lastLoginDt = new Date(await AsyncStorageUtil.getLocalStorage(AsyncStorageKeys.KEY_LAST_LOGIN_DT));

    if (tokenInfo !== null && isSameDay(lastLoginDt, new Date())) {
        let profile = await OBRAPIService.getOwnProfile(tokenInfo);
        if (profile == null) {
            await Keychain.resetInternetCredentials(Config.obrTokenKeychainServer);
            return null;
        }
        else {
            return {
                // tokenInfo: tokenInfo,
                profileInfo: profile,
                lastLoginDt: lastLoginDt
            }
        }
    }
    return null;
}
