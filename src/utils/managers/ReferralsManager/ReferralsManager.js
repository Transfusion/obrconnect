
const YAP_CHEE_KEAN_DETAILS = {
    name: 'Yap Chee Kean',
    contact_no: '011-222-3333',
    member_since: new Date('2017/01/01'),
    staff_id: '50670',
    branch: 'Jln. Imbi',
    business_unit: 'Personal Banker'
};

const CONNIE_CHAN_DETAILS = {
    name: 'Connie Chan',
    contact_no: '011-222-3333',
    member_since: new Date('2017/01/01'),
    staff_id: '50670',
    branch: 'Jln. Imbi',
    business_unit: 'Personal Banker'
};

const RAJESH_DETAILS = {
    name: 'Rajesh A/L Aru',
    contact_no: '011-222-3333',
    member_since: new Date('2017/01/01'),
    staff_id: '50670',
    branch: 'Jln. Imbi',
    business_unit: 'Personal Banker'
};

export async function getAllReferrals() {
    return [{
        from: YAP_CHEE_KEAN_DETAILS,
        date: new Date('2018/10/05'),
        lead_company: 'ABC Sdn Bhd',
        lead_company_id: '1234-X',
        lead_contact_person: 'Tan Ah Kow',
        lead_contact_no: '016-xxxxxx',
        lead_contact_nric: '750101-07-xxxx',
        read: false
    },{
        from: CONNIE_CHAN_DETAILS,
        date: new Date('2018/10/03'),
        lead_company: 'DEC Sdn Bhd',
        lead_contact_person: 'Lee Li Li',
        lead_company_id: '1234-X',
        lead_contact_no: '016-xxxxxx',
        lead_contact_nric: '750101-07-xxxx',
        read: true
    },{
        from: RAJESH_DETAILS,
        date: new Date('2018/09/05'),
        lead_company: 'RRC Sdn Bhd',
        lead_contact_person: 'Lim Ah Ba',
        lead_company_id: '1234-X',
        lead_contact_no: '016-xxxxxx',
        lead_contact_nric: '750101-07-xxxx',
        read: true
    }]
}

export async function getLeadsReferred() {
    return [{
        to: YAP_CHEE_KEAN_DETAILS,
        date: new Date('2019/01/10'),
        lead_company: 'ABC Sdn Bhd',
        lead_contact_person: 'Tan Ah Kow',
        lead_company_id: '1234-X',
        lead_contact_no: '016-xxxxxx',
        lead_contact_nric: '750101-07-xxxx',
        status: 'WIP'
    }, {
        to: CONNIE_CHAN_DETAILS,
        date: new Date('2019/01/01'),
        lead_company: 'DEC Sdn Bhd',
        lead_contact_person: 'Lee Li Li',
        lead_company_id: '1234-X',
        lead_contact_no: '016-xxxxxx',
        lead_contact_nric: '750101-07-xxxx',
        status: 'Successful'
    },{
        to: RAJESH_DETAILS,
        date: new Date('2018/12/05'),
        lead_company: 'RRC Sdn Bhd',
        lead_contact_person: 'Lim Ah Ba',
        lead_company_id: '1234-X',
        lead_contact_no: '016-xxxxxx',
        lead_contact_nric: '750101-07-xxxx',
        status: 'Unsuccessful'
    }]
}

export async function getLeadsReceived() {
    return [{
        from: YAP_CHEE_KEAN_DETAILS,
        date: new Date('2019/01/10'),
        lead_company: 'ABC Sdn Bhd',
        lead_contact_person: 'Tan Ah Kow',
        lead_company_id: '1234-X',
        lead_contact_no: '016-xxxxxx',
        lead_contact_nric: '750101-07-xxxx',
        status: 'WIP'
    }, {
        from: CONNIE_CHAN_DETAILS,
        date: new Date('2019/01/01'),
        lead_company: 'DEC Sdn Bhd',
        lead_contact_person: 'Lee Li Li',
        lead_company_id: '1234-X',
        lead_contact_no: '016-xxxxxx',
        lead_contact_nric: '750101-07-xxxx',
        status: 'Successful'
    },{
        from: RAJESH_DETAILS,
        date: new Date('2018/12/05'),
        lead_company: 'RRC Sdn Bhd',
        lead_contact_person: 'Lim Ah Ba',
        lead_company_id: '1234-X',
        lead_contact_no: '016-xxxxxx',
        lead_contact_nric: '750101-07-xxxx',
        status: 'Unsuccessful'
    }]
}

const SAMPLE_PRODUCTS = [
    {name: 'Product A', summary: 'Product A Summary', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur dictum justo vel tortor suscipit fermentum. Etiam a quam in lorem ullamcorper vulputate ac quis dui. Vestibulum luctus ac mi eu elementum. Pellentesque sodales nunc ac pellentesque tristique. Maecenas fringilla neque felis, vitae bibendum turpis suscipit vel. Donec varius ipsum et massa tincidunt ornare. Ut vulputate, risus sit amet rhoncus tempor, ipsum ipsum mattis ipsum, quis lobortis erat nisl vitae est. Vivamus pulvinar ultrices justo sed condimentum. Praesent odio lorem, suscipit nec feugiat nec, elementum at nisi. Suspendisse nec neque neque.\n' +
            '\n' +
            'Maecenas vitae urna non dolor dignissim efficitur in sit amet nulla. Vivamus elementum consectetur tellus nec luctus. Nunc ac leo mollis, facilisis magna iaculis, fringilla eros. Nulla facilisi. Praesent vitae sapien molestie, efficitur lacus non, elementum enim. Proin ut ex tincidunt, dapibus arcu eu, ullamcorper nunc. Mauris pellentesque nisl quis ante commodo dictum.\n' +
            '\n' +
            'Donec vulputate justo a purus pretium condimentum. Fusce sit amet tortor mi. Cras vel felis sodales, pretium libero sed, pellentesque mauris. Sed nec consequat mauris, id porta lacus. Maecenas scelerisque velit in ligula aliquam, a elementum mi pretium. Sed eu lectus aliquam, porttitor magna eu, pellentesque dolor. Donec tellus lectus, facilisis vel accumsan eget, sollicitudin nec ipsum. Donec neque est, efficitur vel lacinia sit amet, pretium ut augue. Donec ut turpis turpis. Donec quis leo condimentum, finibus lectus eu, rutrum urna. Sed diam diam, mollis quis odio vitae, consequat condimentum mauris.\n' +
            '\n' +
            'Maecenas ipsum arcu, tempor eget odio in, vulputate interdum sapien. In vitae fermentum nulla, nec vestibulum orci. Aenean vestibulum sollicitudin dictum. Proin ac ipsum malesuada, vehicula sapien sit amet, volutpat elit. Morbi ac sapien maximus, dictum lacus eu, commodo augue. Integer quis lectus felis. Praesent accumsan ultricies molestie. Phasellus aliquam ornare libero, vitae feugiat est ultrices at. Praesent luctus nisi et massa consequat iaculis. Fusce eget blandit est. Integer auctor ut felis nec ullamcorper. Nullam finibus, nulla ac mattis consectetur, nunc odio accumsan sapien, ac facilisis enim sapien sit amet nulla. Fusce pharetra, turpis quis fringilla finibus, sapien lacus vulputate quam, non aliquet sapien neque eget orci.'},
    {name: 'Product B', summary: 'Product B Summary', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur dictum justo vel tortor suscipit fermentum. Etiam a quam in lorem ullamcorper vulputate ac quis dui. Vestibulum luctus ac mi eu elementum. Pellentesque sodales nunc ac pellentesque tristique. Maecenas fringilla neque felis, vitae bibendum turpis suscipit vel. Donec varius ipsum et massa tincidunt ornare. Ut vulputate, risus sit amet rhoncus tempor, ipsum ipsum mattis ipsum, quis lobortis erat nisl vitae est. Vivamus pulvinar ultrices justo sed condimentum. Praesent odio lorem, suscipit nec feugiat nec, elementum at nisi. Suspendisse nec neque neque.\n' +
            '\n' +
            'Maecenas vitae urna non dolor dignissim efficitur in sit amet nulla. Vivamus elementum consectetur tellus nec luctus. Nunc ac leo mollis, facilisis magna iaculis, fringilla eros. Nulla facilisi. Praesent vitae sapien molestie, efficitur lacus non, elementum enim. Proin ut ex tincidunt, dapibus arcu eu, ullamcorper nunc. Mauris pellentesque nisl quis ante commodo dictum.\n' +
            '\n' +
            'Donec vulputate justo a purus pretium condimentum. Fusce sit amet tortor mi. Cras vel felis sodales, pretium libero sed, pellentesque mauris. Sed nec consequat mauris, id porta lacus. Maecenas scelerisque velit in ligula aliquam, a elementum mi pretium. Sed eu lectus aliquam, porttitor magna eu, pellentesque dolor. Donec tellus lectus, facilisis vel accumsan eget, sollicitudin nec ipsum. Donec neque est, efficitur vel lacinia sit amet, pretium ut augue. Donec ut turpis turpis. Donec quis leo condimentum, finibus lectus eu, rutrum urna. Sed diam diam, mollis quis odio vitae, consequat condimentum mauris.\n' +
            '\n' +
            'Maecenas ipsum arcu, tempor eget odio in, vulputate interdum sapien. In vitae fermentum nulla, nec vestibulum orci. Aenean vestibulum sollicitudin dictum. Proin ac ipsum malesuada, vehicula sapien sit amet, volutpat elit. Morbi ac sapien maximus, dictum lacus eu, commodo augue. Integer quis lectus felis. Praesent accumsan ultricies molestie. Phasellus aliquam ornare libero, vitae feugiat est ultrices at. Praesent luctus nisi et massa consequat iaculis. Fusce eget blandit est. Integer auctor ut felis nec ullamcorper. Nullam finibus, nulla ac mattis consectetur, nunc odio accumsan sapien, ac facilisis enim sapien sit amet nulla. Fusce pharetra, turpis quis fringilla finibus, sapien lacus vulputate quam, non aliquet sapien neque eget orci.'}
];

export async function getHistoryLeads() {
    return [{
        from: CONNIE_CHAN_DETAILS,
        date: new Date('2019/01/01'),
        lead_company: 'DEC Sdn Bhd',
        lead_contact_person: 'Lee Li Li',
        lead_company_id: '1234-X',
        lead_contact_no: '016-xxxxxx',
        lead_contact_nric: '750101-07-xxxx',
        status: 'Successful',
        products: SAMPLE_PRODUCTS,
    },{
        from: RAJESH_DETAILS,
        date: new Date('2018/12/05'),
        lead_company: 'RRC Sdn Bhd',
        lead_contact_person: 'Lim Ah Ba',
        lead_company_id: '1234-X',
        lead_contact_no: '016-xxxxxx',
        lead_contact_nric: '750101-07-xxxx',
        status: 'Unsuccessful',
        products: SAMPLE_PRODUCTS
    }];
}
