import {AsyncStorageUtil} from "../../storage/AsyncStorageUtil";
import {AsyncStorageKeys} from "../../config/config";

export async function setBiometricEnabled(bool: boolean) {
    return await AsyncStorageUtil.setLocalStorage(AsyncStorageKeys.KEY_BIOMETRIC_ENABLED, bool);
}

export async function getBiometricEnabled() {
    return await AsyncStorageUtil.getLocalStorage(AsyncStorageKeys.KEY_BIOMETRIC_ENABLED);
}
