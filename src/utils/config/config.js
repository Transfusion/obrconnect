

export const Config = {
    primaryColor: '#101F79',
    accentColor: '#ED462F',
    mutedColor: '#777',
    // darkPrimaryColor: '#7B1FA2',
    currencyPrefix: 'RM',

    homeScreenIconSize: 50,
    homeScreenIconGap: 5,

    drawerOffset: 0.2,

    obrTokenKeychainServer: 'obr',

    OBRFont: {
        regular: 'Agenda-Light',
        semiBold: 'Agenda-Regular',
        bold: 'Agenda-SemiBold'
    },

    httpRequestsTimeout: 60000
};


import { AsyncStorage } from "react-native";

export const AsyncStorageKeys = {
    KEY_OBR_API_ENDPOINT: 'obr_api_endpoint',

    KEY_LAST_LOGIN_DT: 'last_login_dt',
    KEY_LAUNCHED_AT_LEAST_ONCE: 'launched_at_least_once',

    // {version:  String(DeviceInfo.getVersion()), build: Number(DeviceInfo.getBuildNumber())});
    KEY_APP_VERSION_BUILD: 'obr_app_version_build',

    KEY_BIOMETRIC_ENABLED: 'biometric_enabled'
};

export class ConfigManager {
    constructor() {}
}
