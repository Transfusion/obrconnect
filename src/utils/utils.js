/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 1/5/19 12:09 AM
 */

/** used when summing a list of numbers **/
export const partialSum = (partial_sum, a) => partial_sum + a;

/** used in the 5 screens actually displaying the policy **/
export function isEmpty(object) {return !!object && Object.values(object).every(x => (x === null || x === ''));}

export function isSameDay(dateToCheck: Date, actualDate: Date){
    if (!dateToCheck || !actualDate) {
        return false;
    }

    return (dateToCheck.getDate() === actualDate.getDate()
        && dateToCheck.getMonth() === actualDate.getMonth()
        && dateToCheck.getFullYear() === actualDate.getFullYear())
}

// https://stackoverflow.com/questions/46946380/fetch-api-request-timeout
export function timeout(ms, promise) {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            reject(new Error("timeout"))
        }, ms);

        promise.then(resolve, reject)
    })
}

export function intersperse(items, separator) {
    const result = items.reduce(
        (res, el) => [...res, el, separator], []);
    result.pop();
    return result;
}
