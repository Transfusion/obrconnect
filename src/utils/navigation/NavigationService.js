/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 11:20 AM
 */

/** Singleton used to allow the Drawer (which isn't part of any navigator) to manipulate the root navigator **/

import { NavigationActions } from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
    _navigator = navigatorRef;
}

function navigate(routeName, params) {

    _navigator.dispatch(
        NavigationActions.navigate({
            routeName,
            params,
        })
    )

}

export default {
    navigate,
    setTopLevelNavigator
}