/**
 * Export for TMLM specific icons
 */

import {createIconSetFromIcoMoon} from 'react-native-vector-icons';


import icomoonConfig from './fonts/selection.json';
const Icon = createIconSetFromIcoMoon(icomoonConfig);

export default Icon;