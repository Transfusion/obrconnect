/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 4:04 PM
 */


import React, {Component} from "react";

const images = {
    uobLogo: require('./images/uob_logo_transparent.png'),
    userBg: require("./images/banner-generic-uob-cropped.png"),
    obrSlide: require('./images/obr_slide.png'),
    user03: require('./images/user_03.jpg')
};

export default images;
