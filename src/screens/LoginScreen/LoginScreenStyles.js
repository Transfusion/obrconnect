/*
 * Created by Bryan Kok (bryan.wyern1@gmail.com) on 12/5/18 4:23 PM
 */

import React, {StyleSheet, Platform, Dimensions, PixelRatio} from "react-native";
export const LoginScreenStyles = StyleSheet.create({
    imgWrap: {
        width: 150,
        // borderColor: '#000',
        // borderWidth: 2,
        height: 80
    },

    loginContainer: {
        paddingLeft: 30,
        paddingRight: 30,
    }
});
