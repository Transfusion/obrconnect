import {connect} from "react-redux";
import React, {Component} from "react";
import {View, SafeAreaView, KeyboardAvoidingView, Platform, Image, Alert, Text} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import Icon from 'react-native-vector-icons/FontAwesome';

import {PreloaderModal} from "../../components/PreloaderModal";
import {LoginScreenStyles} from "./LoginScreenStyles";
import images from "../../assets/images";
import {Config} from "../../utils/config/config";
import {ThemedTextInput} from "../../components/ThemedTextInput";
import {OBRButton} from "../../components/OBRButton/OBRButton";
import {timeout} from "../../utils/utils";

import {login} from "../../reducers/authReducer";

class _LoginScreen extends Component {

    state = {
        attemptingLogin: false,
        loginFailed: false,

        username: '',
        password: ''
    };

    static navigationOptions = {
        header: null // !!! Hide Header
    };


    constructor(props) {
        super(props);
    }

    _onPreloaderModalDismiss() {
        if (this.state.loginFailed) {
            Alert.alert("Login Failed", this.state.loginFailedMessage);
        }
    }

    async _attemptLogin() {
        if (this.state.username === '' || this.state.password === '') {
            this.setState({ attemptingLogin: false });
            Alert.alert("Alert", "Username and password are required", [
                { text: "OK", onPress: () => { this.setState({ attemptingLogin: false })}}
            ]);
        } else {
            this.setState({attemptingLogin: true, loginFailed: false});
            try {
                let loginResult = await timeout(Config.httpRequestsTimeout, this.props.login(this.state.username, this.props.userId, this.state.password));
                // todo: proper error handling strategy
            }
            catch (error) {
                console.log(error);
                if (Platform.OS === 'android'){
                    Alert.alert('Connection Error', 'Please check your network connection.', [
                        { text: "OK", onPress: () => {
                                this.setState({loginFailedMessage: 'Please check your network connection.', attemptingLogin: false, loginFailed: true
                                })}}
                    ])
                } else
                {this.setState({loginFailedMessage: 'Please check your network connection.', attemptingLogin: false, loginFailed: true});}
            }

            this.setState({
                username: '',
                password: ''
            });
        }
    }

    // Depend on redux updating to navigate.
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (!!this.props.profileInfo) {
            this.props.navigation.navigate('App');
        }
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1, justifyContent: 'center'}}>
            <KeyboardAvoidingView keyboardVerticalOffset={Platform.OS === 'android' ? 50 :10}  behavior="position" enabled>
                <PreloaderModal loading={this.state.attemptingLogin}
                                onDismiss={this._onPreloaderModalDismiss.bind(this)} />
                <View style={{alignItems: 'center'}}>
                    <Image style={LoginScreenStyles.imgWrap} resizeMode={'contain'} source={images.uobLogo} />
                </View>
                <View style={{height: 20}} />
                <View style={LoginScreenStyles.loginContainer}>
                    <Text style={{fontWeight: 'bold', fontSize: 30, fontFamily: Config.OBRFont.bold, color: Config.primaryColor}}>OBR Connect</Text>
                    <View style={{height: 5}} />
                    <View style={{flexDirection: 'row'}}>
                        <View style={{borderRadius: 50, height: 40, alignItems: 'center', justifyContent: 'center',
                            backgroundColor: Config.primaryColor}}>
                            <Icon style={{padding: 5}} color={'#fff'} size={25} name={'id-card-o'} />
                        </View>
                        <View style={{width: 10}}/>
                        <ThemedTextInput
                            wrapperStyle={{flex: 1}}
                            style={{flex: 1}}
                            animatedTextInputRef={ref => this._usernameTextInput = ref}
                            placeholder={'LAN ID'}
                            value={this.state.username}
                            onChangeText={text => {
                                this.setState({
                                    username: text
                                })
                            }}/>

                        <View style={{width: 10}}/>

                    </View>

                    <View style={{height: 20}}/>

                    <View style={{flexDirection: 'row'}}>
                        <View style={{borderRadius: 50, height: 40, alignItems: 'center', justifyContent: 'center',
                            backgroundColor: Config.primaryColor}}>
                            <Icon style={{padding: 5}} color={'#fff'} size={28} name={'key'} />
                        </View>
                        <View style={{width: 10}}/>

                        <ThemedTextInput
                            secureTextEntry={true}
                            wrapperStyle={{flex: 1}}
                            style={{flex: 1}}
                            animatedTextInputRef={ref => this._passwordTextInput = ref}
                            placeholder={'Password'}
                            value={this.state.password}
                            onChangeText={text => {
                                this.setState({
                                    password: text
                                })
                            }}/>

                        <View style={{width: 10}}/>
                    </View>

                    <View style={{height: 20}}/>
                    <OBRButton onPress={this._attemptLogin.bind(this)} text={'Login'}/>
                    <View style={{height: 40}} />

                </View>
            </KeyboardAvoidingView>
                <Text style={{ position: "absolute", bottom: 25, marginLeft: 30, fontFamily: Config.OBRFont.regular }}>Version {DeviceInfo.getVersion()}</Text>
                <Text style={{ position: "absolute", bottom: 10, marginLeft: 30, fontFamily: Config.OBRFont.regular }}>Build {DeviceInfo.getBuildNumber()}</Text>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
        profileInfo: state.authReducer.profileInfo,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...ownProps,
        login: (username, userId, password) => {
            return dispatch(login(username, password));
        },
    }
};

export const LoginScreen = connect(mapStateToProps, mapDispatchToProps)(_LoginScreen);
