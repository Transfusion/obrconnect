
import React, {Component} from "react";
import {Switch, Text, View} from 'react-native';
import {connect} from "react-redux";
import {Config} from "../../utils/config/config";

import {SharedDrawerConsumer} from "../../providers/SharedDrawerContext";
import Icon from "../../assets/tmlmIcons";
import {getBiometricEnabled, setBiometricEnabled} from "../../utils/managers/SettingsManager/SettingsManager";

class _SettingsScreen extends Component {
    constructor(props) {
        super(props);

    }
    state = {
        biometricEnabled: false,
    };

    static navigationOptions = ({ navigation, screenProps }) => {
        const params = navigation.state.params || {};
        return {
            title: 'Settings',
            headerBackTitle: 'Back',
            headerStyle: {
                backgroundColor: Config.primaryColor,
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'normal',
                fontSize: 18,
                fontFamily: Config.OBRFont.semiBold
            },
            headerRight: <SharedDrawerConsumer>
                {({ drawerOpened }) => (
                    <Icon
                        onPress = {() => {
                            drawerOpened(true);
                        }}
                        style={{paddingRight: 20}} name='icon_menu' color={'#fff'} />
                )}

            </SharedDrawerConsumer>
        }
    };

    componentWillMount(): void {
        getBiometricEnabled().then(enabled => {
            this.setState({biometricEnabled: !!enabled});
        })
    }

    async _onBiometricSwitchToggle(val) {
        this.setState({biometricEnabled: val}, async () => {
            await setBiometricEnabled(val);
        });
    }

    render() {
        return (
            <View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 15, borderBottomColor: Config.mutedColor, borderBottomWidth: 1}}>
                    <Text style={{fontFamily: Config.OBRFont.bold}}>Enable biometric quick unlock if available</Text>
                    <Switch value={this.state.biometricEnabled} onValueChange={this._onBiometricSwitchToggle.bind(this)}/>
                </View>
            </View>
        );
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...ownProps,
    }
};

export const SettingsScreen = connect(mapStateToProps, mapDispatchToProps)(_SettingsScreen);
