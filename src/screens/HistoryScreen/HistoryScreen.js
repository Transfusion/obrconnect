
import React, {Component} from "react";
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import {connect} from "react-redux";
import {Config} from "../../utils/config/config";
import {SharedDrawerConsumer} from "../../providers/SharedDrawerContext";
import Icon from "../../assets/tmlmIcons";
import {getHistoryLeads} from "../../utils/managers/ReferralsManager/ReferralsManager";
import {PreloaderModal} from "../../components/PreloaderModal";
import moment from "moment";
import {HeaderBackButton} from "react-navigation";

class _HistoryScreen extends Component {
    constructor(props) {
        super(props);

    }

    state = {
        isFetchingHistoryLeads: false,
        historyLeads: null
    };

    static navigationOptions = ({ navigation, screenProps }) => {
        const params = navigation.state.params || {};
        return {
            title: 'History',
            headerBackTitle: 'Back',
            headerStyle: {
                backgroundColor: Config.primaryColor,
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'normal',
                fontSize: 18,
                fontFamily: Config.OBRFont.semiBold
            },
            headerLeft:(<HeaderBackButton title={'Back'} tintColor={'#fff'} onPress={()=>{navigation.navigate('HomeScreen')}}/>),
            headerRight: <SharedDrawerConsumer>
                {({ drawerOpened }) => (
                    <Icon
                        onPress = {() => {
                            drawerOpened(true);
                        }}
                        style={{paddingRight: 20}} name='icon_menu' color={'#fff'} />
                )}

            </SharedDrawerConsumer>
        }
    };

    componentWillMount(): void {
        this._fetchHistoryLeads();
    }

    async _fetchHistoryLeads() {
        let historyLeads = await getHistoryLeads();
        this.setState({historyLeads: historyLeads})
    }

    _renderStatus(status) {
        switch(status) {
            // TODO: should never have a lead with WIP status on this screen.
            case 'WIP':
                return <View style={{padding: 5,borderRadius: 10,  backgroundColor:'#EF6230'}}><Text style={{color: '#fff'}}>{status}</Text></View>;
            case 'Successful':
                return <View style={{padding: 5, borderRadius: 10, backgroundColor:'#377C4D'}}><Text style={{color: '#fff'}}>{status}</Text></View>;
            case 'Unsuccessful':
                return <View style={{padding: 5, borderRadius: 10, backgroundColor:'#DE412C'}}><Text style={{color: '#fff'}}>{status}</Text></View>;
        }
    }

    _renderHistoryLead({item, index}) {
        return (
            <TouchableOpacity
                onPress={() => {this.props.navigation.navigate('HistoryCaseDetailsScreen', {caseDetails: item, sending: false})}}
                style={{padding: 15, paddingLeft: 25, borderBottomColor: Config.mutedColor, borderBottomWidth: 1}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    {/*Title*/}
                    <Text style={{fontWeight: 'bold', fontSize: 18, fontFamily: Config.OBRFont.bold}}>{item.from.name}</Text>
                    <Text style={{fontFamily: Config.OBRFont.bold, color: Config.mutedColor}}>{moment(item.date).format('YYYY/MM/DD HH:mm')}</Text>
                </View>

                <View style={{flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-between'}}>
                    <View>
                        <Text style={{fontSize: 16, fontFamily: Config.OBRFont.semiBold}}>{item.lead_company}</Text>
                        <Text style={{fontSize: 12, fontFamily: Config.OBRFont.regular}}>{item.lead_contact_person}, {item.lead_contact_no}</Text>
                    </View>
                    {this._renderStatus(item.status)}
                </View>
            </TouchableOpacity>
        )
    }

    _onPreloaderModalDismiss() {

    }

    render() {
        return (
            <View style={{flex: 1}}>
                <PreloaderModal loading={this.state.isFetchingHistoryLeads}
                                onDismiss={this._onPreloaderModalDismiss.bind(this)} />
                <FlatList data={this.state.historyLeads} renderItem={this._renderHistoryLead.bind(this)}/>
            </View>
        );
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...ownProps,
    }
};

export const HistoryScreen = connect(mapStateToProps, mapDispatchToProps)(_HistoryScreen);
