import {Config} from "../../utils/config/config";
import {SharedDrawerConsumer} from "../../providers/SharedDrawerContext";
import Icon from "../../assets/tmlmIcons";
import {connect} from "react-redux";
import React, {Component} from "react";
import {View, Text, TouchableOpacity, ScrollView, Platform, KeyboardAvoidingView} from 'react-native';
import {OBRBorderedBoxWithTitle} from "../../components/OBRBorderedBoxWithTitle";
import {DetailsRow} from "../../components/DetailsRow/DetailsRow";

import EntypoIcon from 'react-native-vector-icons/Entypo';
import AntIcon from 'react-native-vector-icons/AntDesign';
import {ForwardModal} from "./ForwardModal/ForwardModal";

const divider = <View style={{marginTop: 15, marginBottom: 15,
    height: 1, borderRadius: 5, backgroundColor: '#ddd'}} />;

class _CaseDetailsScreen extends Component {

    state = {
        forwardModalVisible: false,
    };

    static navigationOptions = ({ navigation, screenProps }) => {
        const params = navigation.state.params || {};
        return {
            title: 'Case Details',
            headerBackTitle: 'Back',
            headerStyle: {
                backgroundColor: Config.primaryColor,
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                // fontWeight: 'normal',
                fontSize: 18,
                fontFamily: Config.OBRFont.semiBold
            },
            headerRight: <SharedDrawerConsumer>
                {({ drawerOpened }) => (
                    <Icon
                        // onPress={() => {ManageProfileNavigator.eventEmitter.emit('hamburger_pressed')}}
                        onPress = {() => {
                            drawerOpened(true);
                        }}
                        style={{paddingRight: 20}} name='icon_menu' color={'#fff'} />
                )}

            </SharedDrawerConsumer>
        }
    };

    constructor(props){
        super(props);
    }

    _performForward(){

    }

    render() {
        // TODO: this is for POC purposes only
        // let caseDetails = this.props.navigation.getParam('caseDetails', null);

        return <View style={{flex: 1}}>
            <ForwardModal visible={this.state.forwardModalVisible}
                          onBackdropPress={() => {this.setState({forwardModalVisible: false})}}/>

            <ScrollView style={{flex: 1, padding: 15}}>
                <OBRBorderedBoxWithTitle title={'Referrer Details'}>
                    <DetailsRow left={'Referral Name'} right={'Yap Chee Kean'} />
                    {divider}
                    <DetailsRow left={'Staff ID'} right={'50670'} />
                    {divider}
                    <DetailsRow left={'Segments'} right={'Personal Banker'} />
                    {divider}
                    <DetailsRow left={'Branch'} right={'Jln. Imbi'} />
                </OBRBorderedBoxWithTitle>
                <View style={{height: 10}}/>
                <OBRBorderedBoxWithTitle title={'Lead Details'}>
                    <DetailsRow left={'Company Name'} right={'ABC Sdn. Bhd.'} />
                    {divider}
                    <DetailsRow left={'Company ID'} right={'1234-X'} />
                    {divider}
                    <DetailsRow left={'Contact Person Name'} right={'Tan Ah Kow'} />
                    {divider}
                    <DetailsRow left={'Contact Person NRIC'} right={'750101-07-xxxx'} />
                    {divider}
                    <DetailsRow left={'Contact Number'} right={'016-456xxxx'} />
                </OBRBorderedBoxWithTitle>
                <View style={{height: 10}}/>
                <OBRBorderedBoxWithTitle title={'Remarks'}>
                    <Text selectable style={{fontFamily: Config.OBRFont.regular}}>
                        Hey, kindly contact the above for opening of current account. He will be available after 2pm tomorrow
                    </Text>
                </OBRBorderedBoxWithTitle>
                <View style={{height: 30}} />
            </ScrollView>

            <View style={{flexDirection: 'row', padding: 15, paddingBottom: 30}}>
                <TouchableOpacity onPress={() => {this.props.navigation.navigate('LeadsReferredScreen',
                    {targetRoute: 'LeadsReceivedScreen'})}} style={{flexDirection: 'row', flex: 1,  borderRadius: 10, backgroundColor: '#377C4D', padding: 10, alignItems: 'center', justifyContent: 'space-between'}}>
                    <AntIcon name={'checkcircle'} size={25} color={'#fff'}/>
                    <View style={{width: 3}} />
                    <Text style={{color: '#fff', fontSize: 20, fontFamily: Config.OBRFont.semiBold}}>Accept</Text>
                </TouchableOpacity>
                <View style={{width: 10}}/>
                <TouchableOpacity onPress={() => {this.props.navigation.navigate('HistoryScreen')}} style={{flexDirection: 'row', flex: 1,  borderRadius: 10, backgroundColor: '#DE412C', padding: 10, alignItems: 'center', justifyContent: 'space-between'}}>
                    <AntIcon name={'closecircle'} size={25} color={'#fff'}/>
                    <Text style={{color: '#fff', fontSize: 20, fontFamily: Config.OBRFont.semiBold}}>Decline</Text>
                </TouchableOpacity>
                <View style={{width: 10}}/>
                <TouchableOpacity onPress={() => {this.setState({forwardModalVisible: true})}} style={{flexDirection: 'row', flex: 1,  borderRadius: 10, backgroundColor: Config.primaryColor, padding: 10, alignItems: 'center', justifyContent: 'space-between'}}>
                    <EntypoIcon name={'forward'} size={25} color={'#fff'}/>
                    <Text style={{color: '#fff', fontSize: 20, fontFamily: Config.OBRFont.semiBold}}>Forward</Text>
                </TouchableOpacity>


            </View>
        </View>
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...ownProps,
    }
};

export const CaseDetailsScreen = connect(mapStateToProps, mapDispatchToProps)(_CaseDetailsScreen);
