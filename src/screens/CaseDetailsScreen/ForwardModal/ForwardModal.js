import React, {Component} from 'react';
import {View, Picker, Text, Platform, KeyboardAvoidingView, Alert} from 'react-native';
import Modal from 'react-native-modal';
import {connect} from "react-redux";
import {Config} from "../../../utils/config/config";
import styles from './styles';
import {Dropdown} from "react-native-material-dropdown";
import {ThemedTextInput} from "../../../components/ThemedTextInput";
import {OBRButton} from "../../../components/OBRButton/OBRButton";
import {PreloaderModal} from "../../../components/PreloaderModal";

/** Modal to select which colleague to forward to **/
export class ForwardModal extends Component {
    constructor(props){super(props);}

    state = {
        attemptingForward: false,

        branch: null,
        segment: null,
        person: null,

        notes: '',

        forwardFailed: false
    };

    async _performForward() {
        this.setState({attemptingForward: true});
        if (!this.state.branch || !this.state.segment || !this.state.person) {
            this.setState({attemptingForward: false}, () => {
                alert("Please select a person to forward to.")
            });
        }
        setTimeout(() => {
            this.setState({attemptingForward: false});
        }, 500)

    }

    _onPreloaderModalDismiss() {
        /*if (this.state.loginFailed) {
            Alert.alert("Login Failed", this.state.loginFailedMessage);
        }*/
        if (!this.state.forwardFailed) {
            alert('Successfully submitted.');
        } else {alert('Failed to submit. Please try again.')}
    }

    render(){
        const {visible, onModalHide, onBackdropPress} = this.props;
        return <Modal onModalHide={onModalHide} onBackdropPress={onBackdropPress} useNativeDriver={true} isVisible={visible}>

            <PreloaderModal loading={this.state.attemptingForward}
                            onDismiss={this._onPreloaderModalDismiss.bind(this)} />

            <KeyboardAvoidingView keyboardVerticalOffset={Platform.OS === 'android' ? 10 : 30}  behavior="position" enabled>
            <View style={styles.modalContent}>
                <Text style={{fontSize: 15, fontFamily: Config.OBRFont.semiBold}}>Forward this case to:</Text>

                <View style={{width: '90%'}}>

                    <Dropdown onChangeText={(value, index, data) => {this.setState({branch: value})}} label={'Branch'} data={[{value: 'Bangsar'}, {value: 'Jln. Imbi'}]} value={this.state.branch ? this.state.branch : ''} />
                    <Dropdown disabled={!this.state.branch} onChangeText={(value, index, data) => {this.setState({segment: value})}} label={'Segment'} data={[{value: 'Personal Banking'}, {value: 'Corporate Banking'}]} value={this.state.segment ? this.state.segment : ''}  />
                    <Dropdown disabled={!this.state.segment} onChangeText={(value, index, data) => {this.setState({person: value})}} label={'Officer'} data={[{value: 'Colleague A'}, {value: 'Colleague B'}]} value={this.state.person ? this.state.person : ''}  />

                    <Text style={{fontFamily: Config.OBRFont.regular}}>Notes</Text>
                        <ThemedTextInput multiline keyboardType={'default'} style={{height: 50}} value={this.state.notes} onChangeText={text => this.setState({notes: text})}/>
                    <View style={{height: 10}} />
                        <OBRButton disabled={!this.state.branch || !this.state.segment || !this.state.person} text={'Forward'} onPress={this._performForward.bind(this)}/>

                        <View style={{height: 10}} />
                </View>
            </View>
            </KeyboardAvoidingView>
        </Modal>
    }
}
