
import {StyleSheet} from 'react-native';
import {Config} from "@utils/config/config";


const styles = StyleSheet.create({
    slider: {
        backgroundColor: 'transparent'
    },
    slide1: {
        flex: 1,
        margin: Config.homeScreenIconGap,
        justifyContent: 'center',
        // alignItems: 'center',
        // backgroundColor: '#9DD6EB',
    },
    slideBtnRow: {flex: 1, flexDirection: 'row'},
    slide2: {
        flex: 1,
        margin: Config.homeScreenIconGap,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#97CAE5',
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    }
});

export default styles;