import {connect} from "react-redux";
import React, {Component} from "react";
import {View, Text} from 'react-native';
import Swiper from 'react-native-swiper';

import Ionicons from 'react-native-vector-icons/Ionicons';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import AntIcon from 'react-native-vector-icons/AntDesign';

import {SharedDrawerConsumer} from "../../providers/SharedDrawerContext";
import Icon from "../../assets/tmlmIcons";
import {Config} from "../../utils/config/config";
import {HomeSlideshow} from "../../components/HomeSlideshow";
import {HomeScreenBtn} from "../../components/HomeScreenBtn";

import styles from './HomeScreenStyles'
import {Screen} from "@shoutem/ui";

class _HomeScreen extends Component {

    static navigationOptions = ({ navigation, screenProps }) => {
        const params = navigation.state.params || {};
        return {
            title: 'OBR Connect',
            headerBackTitle: 'Back',
            headerStyle: {
                backgroundColor: Config.primaryColor,
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                // fontWeight: 'normal',
                fontSize: 18,
                fontFamily: Config.OBRFont.semiBold
            },
            headerRight: <SharedDrawerConsumer>
                {({ drawerOpened }) => (
                    <Icon
                        // onPress={() => {ManageProfileNavigator.eventEmitter.emit('hamburger_pressed')}}
                        onPress = {() => {
                            drawerOpened(true);
                        }}
                        style={{paddingRight: 20}} name='icon_menu' color={'#fff'} />
                )}

            </SharedDrawerConsumer>
        }
    };

    constructor(props) {
        super(props);
    }

    render() {
        const navigation = this.props.navigation;
        let groupedButtonsFirstPane = [
            // first row
            [{
                icon: <Ionicons color={Config.primaryColor} name="ios-mail-unread" size={Config.homeScreenIconSize}/>,
                badgeText: '1',
                text: 'Receive Referral',
                // imageColor: '#0096a9',
                buttonHeight: 100,
                onPress: () => {navigation.navigate('ReceiveReferralsScreen')}
            }, {
                icon: <Ionicons color={Config.primaryColor} name="ios-list-box" size={Config.homeScreenIconSize}/>,
                text: 'My Worklist',
                // imageColor: '#0096a9',
                buttonHeight: 100,
                onPress: () => {navigation.navigate('MyWorklistScrollableTabNavigator')}
            }],

            [{
                icon: <Ionicons color={Config.primaryColor} name="ios-redo" size={Config.homeScreenIconSize}/>,
                text: 'Make a referral',
                buttonHeight: 100,
                onPress: () => {navigation.navigate('MakeReferralScreen')}
            },{
                icon: <FAIcon color={Config.primaryColor} name="history" size={Config.homeScreenIconSize}/>,
                text: 'History',
                buttonHeight: 100,
                onPress: () => {navigation.navigate('HistoryScreen')}
            }],
            [{
                icon: <AntIcon color={Config.mutedColor} name="copy1" size={Config.homeScreenIconSize}/>,
                text: 'My Team OBR Report',
                buttonHeight: 100,
                onPress: () => {navigation.navigate('SampleChartsDemo')}
            },{
                icon: 'dummy',
                /*text: 'Tools',
                buttonHeight: 100,
                linkTo: null,
                onPress: () => {navigation.navigate('ToolsScreen')}*/
            }]

        ];

        let firstSlideBtns = groupedButtonsFirstPane.map(btns => {
            return (
                <View style={styles.slideBtnRow}>
                    {btns.map(btnInfo => {
                        if (btnInfo.icon === 'dummy') {
                            return <View style={{flex: 1, padding: 15}}/>
                        }
                        return <HomeScreenBtn
                            badgeText={btnInfo.badgeText}
                            onPress={btnInfo.onPress}
                            icon={btnInfo.icon}
                            buttonHeight={btnInfo.buttonHeight}
                            text={btnInfo.text}/>
                    })}
                </View>
            )
        });

        return (
            <Screen>
            <View style={{flex: 1}}>
                <HomeSlideshow style={{flex: 0.65}}/>

                <Swiper style={styles.slider} showsButtons={true} showsPagination={false}
                        loop={false}>
                    <View style={styles.slide1}>
                        {firstSlideBtns}
                    </View>

                    {/*<View style={styles.slide2}>
                            {secondSlideBtns}
                        </View>*/}
                </Swiper>

            </View>
            </Screen>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...ownProps,
    }
};

export const HomeScreen = connect(mapStateToProps, mapDispatchToProps)(_HomeScreen);
