/** Decide to implement the splash screen in RN for future animation support **/
import React, {Component} from "react";
import {Dimensions, AsyncStorage, Platform, Animated, Image, Alert, View} from "react-native";

import {connect} from "react-redux";

import {checkLoggedIn, logout} from "@reducers/authReducer";
import images from "@assets/images";
import {getBiometricEnabled} from "../../utils/managers/SettingsManager/SettingsManager";
import TouchID from "react-native-touch-id";
import * as Keychain from "react-native-keychain";
import {AsyncStorageUtil} from "../../utils/storage/AsyncStorageUtil";
import DeviceInfo from "react-native-device-info";
import {AsyncStorageKeys, Config} from "../../utils/config/config";
import compareVersions from "compare-versions";


class _SplashScreen extends Component {
    static navigationOptions = ({navigation, screenProps}) => {

    };

    state = {
        fadeAnim: new Animated.Value(0),  // Initial value for opacity: 0
        screenDims: Dimensions.get('window')
    };

    componentDidMount() {
        Animated.timing(                  // Animate over time
            this.state.fadeAnim,            // The animated value to drive
            {
                toValue: 1,                   // Animate to opacity: 1 (opaque)
                duration: 800,              // Make it take a while
            }
        ).start();                        // Starts the animation

        (async () => {
            // check whether the app has been up or downgraded.
            let versionBuildInfo = await AsyncStorageUtil.getLocalStorage(AsyncStorageKeys.KEY_APP_VERSION_BUILD, null);
            if (versionBuildInfo) {
                let {version, build} = versionBuildInfo;

                let currentVersion = String(DeviceInfo.getVersion());
                let currentBuild = Number(DeviceInfo.getBuildNumber());
                if (compareVersions(currentVersion, version) !== 0 || currentBuild !== build) {
                    // If the versions differ,
                    await AsyncStorage.clear();
                    await Keychain.resetInternetCredentials(Config.obrTokenKeychainServer);
                }
            } else {
                await AsyncStorage.clear();
                await Keychain.resetInternetCredentials(Config.obrTokenKeychainServer);
            }

            await AsyncStorageUtil.setLocalStorage(AsyncStorageKeys.KEY_APP_VERSION_BUILD,
                {version:  String(DeviceInfo.getVersion()), build: Number(DeviceInfo.getBuildNumber())});
            await this._checkLoggedIn();
        })()

    }

    async _checkLoggedIn() {
        let loggedIn = await this.props.checkLoggedIn();
        let biometricEnabled = await getBiometricEnabled();
        if (loggedIn && biometricEnabled){
            try {
                await TouchID.authenticate('Biometric Auth for OBRConnect', {passcodeFallback: true});
            }
            catch (error) {
                Alert.alert('Biometric authentication failed.', Platform.OS === 'android' ?
                    error.code : error.message);
                await this.props.logout();
                this.props.navigation.navigate('LoginScreen');
                return
            }
        }

        setTimeout(() => {
            // invariant: if the prompt to update dialog is already shown that means appMetadata
            // must be populated.
            this.props.navigation.navigate(loggedIn ? 'HomeScreen' : 'LoginScreen');
        }, 1000);
    }

    _dimensionsChangeHandler(dims) {
        this.setState({screenDims: dims.screen});
    }

    componentWillMount() {
        Dimensions.addEventListener("change", this._dimensionsChangeHandler.bind(this));
    }


    componentWillUnmount() {
        // Important to stop updating state after unmount
        Dimensions.removeEventListener("change", this._dimensionsChangeHandler.bind(this));
    }


    render() {
        const {width, height} = this.state.screenDims;

        return <Animated.View style={{opacity: this.state.fadeAnim, flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Image style={{width: 0.5 * width}} resizeMode={'contain'} source={images.uobLogo} />
        </Animated.View>
    }

}


const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...ownProps,
        logout: () => {return dispatch(logout()) },
        checkLoggedIn: () => {
            return dispatch(checkLoggedIn())
        },
    }
};

export const SplashScreen = connect(mapStateToProps, mapDispatchToProps)(_SplashScreen);
