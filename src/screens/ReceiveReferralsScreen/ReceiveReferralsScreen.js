import {Config} from "../../utils/config/config";
import {SharedDrawerConsumer} from "../../providers/SharedDrawerContext";
import {connect} from "react-redux";
import Icon from "../../assets/tmlmIcons";
import React, {Component} from "react";
import {View, Text, FlatList, TouchableOpacity} from 'react-native';

import FAIcon from 'react-native-vector-icons/FontAwesome';
import {getAllReferrals} from "../../utils/managers/ReferralsManager/ReferralsManager";
import {PreloaderModal} from "../../components/PreloaderModal";
import moment from "moment";

const divider = <View style={{marginTop: 15, marginBottom: 15,
    height: 1, borderRadius: 5, backgroundColor: '#ddd'}} />;

const redDot = <View style={{height: 15, width: 15, borderRadius: 100, backgroundColor: Config.accentColor,
    position: 'absolute', top: 8, left: 8}} />;

class _ReceiveReferralsScreen extends Component {

    state = {
        isFetchingReferrals: false,
        referrals: null
    };

    static navigationOptions = ({ navigation, screenProps }) => {
        const params = navigation.state.params || {};
        return {
            title: 'Referrals',
            headerBackTitle: 'Back',
            headerStyle: {
                backgroundColor: Config.primaryColor,
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'normal',
                fontSize: 18,
                fontFamily: Config.OBRFont.semiBold
            },
            headerRight: <SharedDrawerConsumer>
                {({ drawerOpened }) => (
                    <Icon
                        onPress = {() => {
                            drawerOpened(true);
                        }}
                        style={{paddingRight: 20}} name='icon_menu' color={'#fff'} />
                )}

            </SharedDrawerConsumer>
        }
    };

    constructor(props) {
        super(props);
    }

    componentWillMount(): void {
        this._fetchReferrals();
    }

    async _fetchReferrals() {
        this.setState({isFetchingReferrals: true});
        let referrals = await getAllReferrals();
        this.setState({isFetchingReferrals: false, referrals: referrals});
    }

    _onPreloaderModalDismiss() {

    }

    _renderReferral({item, index}) {

        return (
            <TouchableOpacity
                onPress={() => {this.props.navigation.navigate('CaseDetailsScreen', {caseDetails: item})}}
                style={{padding: 15, paddingLeft: 25, borderBottomColor: Config.mutedColor, borderBottomWidth: 1}}>
                {!item.read ? redDot : false}
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    {/*Title*/}
                    <Text style={{fontWeight: 'bold', fontSize: 18, fontFamily: Config.OBRFont.bold}}>{item.from.name}</Text>
                    <Text style={{fontFamily: Config.OBRFont.bold, color: Config.mutedColor}}>{moment(item.date).format('YYYY/MM/DD HH:mm')}</Text>
                </View>
                <Text style={{fontSize: 16, fontFamily: Config.OBRFont.semiBold}}>{item.lead_company}</Text>
                <Text style={{fontSize: 12, fontFamily: Config.OBRFont.regular}}>{item.lead_contact_person}, {item.lead_contact_no}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        return <View style={{flex: 1}}>
        <PreloaderModal loading={this.state.isFetchingReferrals}
        onDismiss={this._onPreloaderModalDismiss.bind(this)} />
            <FlatList data={this.state.referrals} renderItem={this._renderReferral.bind(this)}/>
        </View>
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...ownProps,
    }
};

export const ReceiveReferralsScreen = connect(mapStateToProps, mapDispatchToProps)(_ReceiveReferralsScreen);
