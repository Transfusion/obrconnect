import React, {Component} from 'react';
import {View, Text, Platform, KeyboardAvoidingView, Alert} from 'react-native';
import Modal from 'react-native-modal';
import {Config} from "../../../utils/config/config";
import styles from './styles';
import {ThemedTextInput} from "../../../components/ThemedTextInput";
import {OBRButton} from "../../../components/OBRButton/OBRButton";

/** Controlled Component (remark text is kept as state in the HistoryCaseDetailsScreen **/
export class RemarkModal extends Component {
    constructor(props){super(props);}

    render(){
        const {visible, remarkText, onChangeRemarkText, onSubmitPress, onModalHide, onBackdropPress} = this.props;
        return <Modal onModalHide={onModalHide} onBackdropPress={onBackdropPress} useNativeDriver={true} isVisible={visible}>


            <KeyboardAvoidingView keyboardVerticalOffset={Platform.OS === 'android' ? 10 : 30}  behavior="position" enabled>
            <View style={styles.modalContent}>
                <Text style={{fontSize: 15, fontFamily: Config.OBRFont.semiBold}}>Enter your remark below</Text>
                <View style={{height: 10}} />
                <View style={{width: '90%'}}>
                    {/*<Text style={{fontFamily: Config.OBRFont.regular}}>Enter your remark below</Text>*/}
                    <ThemedTextInput multiline keyboardType={'default'} style={{height: 100}} value={remarkText} onChangeText={onChangeRemarkText}/>
                    <View style={{height: 10}} />
                    <OBRButton disabled={remarkText === ''} text={'Submit'} onPress={onSubmitPress}/>
                    <View style={{height: 10}} />
                </View>
            </View>
            </KeyboardAvoidingView>
        </Modal>
    }
}
