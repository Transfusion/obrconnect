import {StyleSheet} from "react-native";

export default StyleSheet.create({
    modalContent: {
        backgroundColor: 'white',
        paddingTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    }
});
