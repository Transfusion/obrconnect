import {Config} from "../../utils/config/config";
import {SharedDrawerConsumer} from "../../providers/SharedDrawerContext";
import Icon from "../../assets/tmlmIcons";
import {connect} from "react-redux";
import React, {Component} from "react";
import {View, Text, TouchableOpacity, ScrollView, Platform, Clipboard, KeyboardAvoidingView} from 'react-native';
import {OBRBorderedBoxWithTitle} from "../../components/OBRBorderedBoxWithTitle";
import {DetailsRow} from "../../components/DetailsRow/DetailsRow";

import EntypoIcon from 'react-native-vector-icons/Entypo';
import AntIcon from 'react-native-vector-icons/AntDesign';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {intersperse} from "../../utils/utils";
import {RemarkModal} from "./RemarkModal/RemarkModal";

const divider = <View style={{marginTop: 15, marginBottom: 15,
    height: 1, borderRadius: 5, backgroundColor: '#ddd'}} />;

class _HistoryCaseDetailsScreen extends Component {

    state = {
        remarkModalVisible: false,
        // array of strings
        remarks: ['Sample Previous Remark'],
        remarkText: ''
    };

    static navigationOptions = ({ navigation, screenProps }) => {
        const params = navigation.state.params || {};
        return {
            title: 'History Case Details',
            headerBackTitle: 'Back',
            headerStyle: {
                backgroundColor: Config.primaryColor,
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'normal',
                fontSize: 18,
                fontFamily: Config.OBRFont.semiBold
            },
            headerRight: <SharedDrawerConsumer>
                {({ drawerOpened }) => (
                    <Icon
                        // onPress={() => {ManageProfileNavigator.eventEmitter.emit('hamburger_pressed')}}
                        onPress = {() => {
                            drawerOpened(true);
                        }}
                        style={{paddingRight: 20}} name='icon_menu' color={'#fff'} />
                )}

            </SharedDrawerConsumer>
        }
    };

    constructor(props){
        super(props);
    }

    _performDuplicate(){

    }

    render() {
        // TODO: this is for POC purposes only
        let caseDetails = this.props.navigation.getParam('caseDetails', null);
        let sending = this.props.navigation.getParam('sending', false);

        let caseDetailsPerson = sending ? caseDetails.to : caseDetails.from;

        return <View style={{flex: 1}}>

            <RemarkModal visible={this.state.remarkModalVisible}
                         remarkText={this.state.remarkText}
                         onSubmitPress={() => {this.setState({remarks: this.state.remarks.concat(this.state.remarkText),
                             remarkText: '', remarkModalVisible: false} )}}
                         onBackdropPress={() => {this.setState({remarkModalVisible: false})}}
                         onChangeRemarkText={text => {this.setState({remarkText: text})}} />

            <ScrollView style={{flex: 1, padding: 15}}>
                <OBRBorderedBoxWithTitle title={'Referrer Details'}>
                    <DetailsRow left={`Forwarded ${sending ? 'to' : 'from'}`} right={caseDetailsPerson.name} />
                    {divider}
                    <DetailsRow left={'Status'} right={caseDetails.status} />
                    {divider}
                    <DetailsRow left={'Staff ID'} right={caseDetailsPerson.staff_id} />
                    {divider}
                    <DetailsRow left={'Segments'} right={caseDetailsPerson.business_unit} />
                    {divider}
                    <DetailsRow left={'Branch'} right={caseDetailsPerson.branch} />
                </OBRBorderedBoxWithTitle>
                <View style={{height: 10}}/>
                <OBRBorderedBoxWithTitle title={'Lead Details'}>
                    <DetailsRow left={'Company Name'} right={caseDetails.lead_company} />
                    {divider}
                    <DetailsRow left={'Company ID'} right={caseDetails.lead_company_id} />
                    {divider}
                    <DetailsRow left={'Contact Person Name'} right={caseDetails.lead_contact_person} />
                    {divider}
                    <DetailsRow left={'Contact Person NRIC'} right={caseDetails.lead_contact_nric} />
                    {divider}
                    <DetailsRow left={'Contact Number'} right={caseDetails.lead_contact_no} />
                </OBRBorderedBoxWithTitle>
                <View style={{height: 10}}/>

                <OBRBorderedBoxWithTitle title={'Products'}>
                    {intersperse(caseDetails.products.map((product, index) => <DetailsRow left={product.name} right={product.summary}/>), divider)}
                </OBRBorderedBoxWithTitle>
                <View style={{height: 10}}/>
                <OBRBorderedBoxWithTitle title={'Remarks'}>

                    {intersperse(this.state.remarks.map(remark => <Text selectable style={{fontFamily: Config.OBRFont.regular}}>
                        {remark}
                    </Text>), divider)}

                    {this.state.remarks.length === 0 ? <Text selectable style={{fontFamily: Config.OBRFont.regular}}>
                            No remarks.
                        </Text> : null}

                </OBRBorderedBoxWithTitle>
                <View style={{height: 30}} />
            </ScrollView>

            <View style={{flexDirection: 'row', padding: 15, paddingBottom: 30}}>

                <TouchableOpacity onPress={() => {this.setState({remarkModalVisible: true})}} style={{flexDirection: 'row', flex: 1,  borderRadius: 10,
                    backgroundColor: '#377C4D', padding: 10, alignItems: 'center', justifyContent: 'space-between'}}>
                    <MaterialIcon name={'speaker-notes'} size={25} color={'#fff'}/>
                    <Text style={{color: '#fff', fontSize: 15, fontFamily: Config.OBRFont.semiBold}}>Add Remark</Text>
                </TouchableOpacity>

                <View style={{width: 10}}/>

                <TouchableOpacity onPress={() => {Clipboard.setString(JSON.stringify(caseDetails));
                    alert('Copied case details to clipboard.');}} style={{flexDirection: 'row', flex: 1,  borderRadius: 10, backgroundColor: Config.primaryColor, padding: 10, alignItems: 'center', justifyContent: 'space-between'}}>
                    <EntypoIcon name={'documents'} size={25} color={'#fff'}/>
                    <Text style={{color: '#fff', fontSize: 15, fontFamily: Config.OBRFont.semiBold}}>Duplicate</Text>
                </TouchableOpacity>


            </View>
        </View>
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...ownProps,
    }
};

export const HistoryCaseDetailsScreen = connect(mapStateToProps, mapDispatchToProps)(_HistoryCaseDetailsScreen);
