import {Config} from "../../utils/config/config";
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    formFieldLabel : {fontFamily: Config.OBRFont.regular, paddingBottom: 5},
    formFieldError : {color: '#ff0000', fontFamily: Config.OBRFont.regular, paddingBottom: 5}
});
