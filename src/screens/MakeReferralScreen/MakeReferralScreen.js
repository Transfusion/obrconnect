import React, {Component} from "react";
import {KeyboardAvoidingView, Alert, Text, ScrollView, Platform, View} from 'react-native';
import {Config} from "../../utils/config/config";
import {SharedDrawerConsumer} from "../../providers/SharedDrawerContext";
import {connect} from "react-redux";
import Icon from "../../assets/tmlmIcons";
import {Dropdown} from "react-native-material-dropdown";
import {OBRButton} from "../../components/OBRButton/OBRButton";
import {OBRBorderedBoxWithTitle} from "../../components/OBRBorderedBoxWithTitle";
import {ThemedTextInput} from "../../components/ThemedTextInput";
import styles from './MakeReferralScreenStyles';

const divider = <View style={{height: 10}}/>;

class _MakeReferralScreen extends Component {

    static navigationOptions = ({ navigation, screenProps }) => {
        const params = navigation.state.params || {};
        return {
            title: 'Make Referral',
            headerBackTitle: 'Back',
            headerStyle: {
                backgroundColor: Config.primaryColor,
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'normal',
                fontSize: 18,
                fontFamily: Config.OBRFont.semiBold
            },
            headerRight: <SharedDrawerConsumer>
                {({ drawerOpened }) => (
                    <Icon
                        // onPress={() => {ManageProfileNavigator.eventEmitter.emit('hamburger_pressed')}}
                        onPress = {() => {
                            drawerOpened(true);
                        }}
                        style={{paddingRight: 20}} name='icon_menu' color={'#fff'} />
                )}

            </SharedDrawerConsumer>
        }
    };

    constructor(props){
        super(props);
    }


    state = {
        uobService: null,
        uobServiceError: false,

        branch: null,
        branchError: false,

        segment: null,
        segmentError: false,

        person: null,
        personError: false,

        customerName: '',
        customerNameError: false,

        // Individual or Company
        customerType: 'Individual',

        contactNo: '',
        contactNoError: false,

        customerIDNo: '',
        customerIDNoError: false,

        companyRegNo: '',
        companyRegNoError: false,

        remarkText: '',

    };

    _attemptSubmit() {
        this.setState({uobServiceError: !this.state.uobService,
        branchError: !this.state.branch,
        segmentError: !this.state.segment,
        personError: !this.state.person,
        customerNameError: this.state.customerName === '',
        contactNoError: this.state.contactNo === '',

        customerIDNoError: this.state.customerIDNo === '' && this.state.customerType === 'Individual',
        companyRegNoError: this.state.companyRegNo === '' && this.state.customerType === 'Company'}, () => {
            if (this.state.uobServiceError || this.state.branchError || this.state.personError
                || this.state.customerNameError || this.state.contactNoError || this.state.customerIDNoError && this.state.customerType === 'Individual'
                || this.state.companyRegNoError && this.state.customerType === 'Company') {
                alert('Please correct the errors in the referral form.');
            } else {
                Alert.alert('Submission', 'Referral created successfully.',
                    [{text: 'OK', onPress: () => {this.props.navigation.navigate('MyWorklistScrollableTabNavigator')}}])
            }
        });

    }

    render() {
        return (
            <KeyboardAvoidingView keyboardVerticalOffset={Platform.OS === 'android' ? 0 : 100}
                                  behavior={Platform.OS === 'ios' ? 'position' : null} enabled>
                <ScrollView style={{padding: 15}}>

                    <OBRBorderedBoxWithTitle title={'Lead Details'}>
                        <Dropdown onChangeText={(value, index, data) => {this.setState({uobService: value})}} label={'UOB Service'} data={[{value: 'Service A'}, {value: 'Service B'}]} value={this.state.uobService ? this.state.uobService : ''} />
                        {this.state.uobServiceError ? <Text style={styles.formFieldError}>Select a service.</Text> : null}
                        <Dropdown onChangeText={(value, index, data) => {this.setState({branch: value})}} label={'Branch'} data={[{value: 'Bangsar'}, {value: 'Jln. Imbi'}]} value={this.state.branch ? this.state.branch : ''} />
                        {this.state.branchError ? <Text style={styles.formFieldError}>Select a branch.</Text> : null}
                        <Dropdown disabled={!this.state.branch} onChangeText={(value, index, data) => {this.setState({segment: value})}} label={'Segment'} data={[{value: 'Personal Banking'}, {value: 'Corporate Banking'}]} value={this.state.segment ? this.state.segment : ''}  />
                        {this.state.segmentError ? <Text style={styles.formFieldError}>Select a segment.</Text> : null}
                        <Dropdown disabled={!this.state.segment} onChangeText={(value, index, data) => {this.setState({person: value})}} label={'Individual RM'} data={[{value: 'Individual 1'}, {value: 'Individual 2'}]} value={this.state.person ? this.state.person : ''}  />
                        {this.state.personError ? <Text style={styles.formFieldError}>Select a person.</Text> : null}
                    </OBRBorderedBoxWithTitle>

                    {divider}

                    <OBRBorderedBoxWithTitle title={'Customer Details'}>
                        <Dropdown onChangeText={(value, index, data) => {this.setState({customerType: value})}} label={'Customer Type'} data={[{value: 'Individual'}, {value: 'Company'}]} value={this.state.customerType ? this.state.customerType : ''} />

                        <Text style={styles.formFieldLabel}>Customer Name</Text>
                        <ThemedTextInput value={this.state.customerName} onChangeText={text => {this.setState({customerName: text})}}/>
                        {this.state.customerNameError ? <Text style={styles.formFieldError}>Customer name cannot be empty.</Text> : null}

                        {divider}
                        <Text style={styles.formFieldLabel}>Contact No.</Text>
                        <ThemedTextInput keyboardType={'phone-pad'} onChangeText={text => {this.setState({contactNo: text})}}/>
                        {this.state.contactNoError ? <Text style={styles.formFieldError}>Contact No. cannot be empty.</Text> : null}
                        {divider}

                        {this.state.customerType === 'Individual' ?
                            <View>
                                <Text style={styles.formFieldLabel}>Customer ID No.</Text>
                                <ThemedTextInput value={this.state.customerIDNo} onChangeText={text => {this.setState({customerIDNo: text})}}/>
                                {this.state.customerIDNoError ? <Text style={styles.formFieldError}>Customer ID No. cannot be empty.</Text> : null}
                            </View>
                        : <View>
                                <Text style={styles.formFieldLabel}>Company Reg. No.</Text>
                                <ThemedTextInput value={this.state.companyRegNo} onChangeText={text => {this.setState({companyRegNo: text})}}/>
                                {this.state.companyRegNoError ? <Text style={styles.formFieldError}>Company Reg No. cannot be empty.</Text> : null}
                            </View>}

                    </OBRBorderedBoxWithTitle>

                    {divider}
                    <OBRBorderedBoxWithTitle title={'Staff details'}>
                        <Text style={styles.formFieldLabel}>Staff Name</Text>
                        <ThemedTextInput value={this.props.profileInfo.name} disabled />

                        {divider}
                        <Text style={styles.formFieldLabel}>Staff ID</Text>
                        <ThemedTextInput value={this.props.profileInfo.staff_id} disabled />

                        {divider}
                        <Text style={styles.formFieldLabel}>Branch</Text>
                        <ThemedTextInput value={this.props.profileInfo.branch} disabled />
                        {divider}
                        <Text style={styles.formFieldLabel}>Business Unit</Text>
                        <ThemedTextInput value={this.props.profileInfo.business_unit} disabled />

                    </OBRBorderedBoxWithTitle>
                    {divider}

                    <OBRBorderedBoxWithTitle title={'Additional Remarks'}>
                        <ThemedTextInput multiline keyboardType={'default'} style={{height: 100}}
                                         value={this.state.remarkText}
                                         onChangeText={text => {this.setState({remarkText: text})}}/>

                    </OBRBorderedBoxWithTitle>
                    <View style={{height: 10}} />
                    <OBRButton text={'Submit'} onPress={this._attemptSubmit.bind(this)}/>
                    <View style={{height: 30}} />
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
        profileInfo: state.authReducer.profileInfo
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...ownProps,
    }
};

export const MakeReferralScreen = connect(mapStateToProps, mapDispatchToProps)(_MakeReferralScreen);

