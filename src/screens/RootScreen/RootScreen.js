import React, {Component} from "react";
import {Alert, AppState, StatusBar, View} from "react-native";
import {connect} from "react-redux";
import {createAppContainer} from "react-navigation";
import {AuthSwitchNavigator} from "../../navigation/AuthSwitchNavigator";

import {Config} from "../../utils/config/config";
import NavigationService from "../../utils/navigation/NavigationService";

const Root = createAppContainer(AuthSwitchNavigator);

class _RootScreen extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar backgroundColor={Config.primaryColor} barStyle="light-content" />

                <Root ref={rootNavigatorRef => {
                    NavigationService.setTopLevelNavigator(rootNavigatorRef);
                }}/>
            </View>
        )
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...ownProps,
    }
};

export const RootScreen = connect(mapStateToProps, mapDispatchToProps)(_RootScreen);
