import React, {Component} from "react";
import {Animated, Dimensions, View, WebView} from 'react-native';
import {Config} from "../../utils/config/config";
import {HeaderBackButton} from "react-navigation";
import {SharedDrawerConsumer} from "../../providers/SharedDrawerContext";
import Icon from "../../assets/tmlmIcons";

export class SampleChartsDemo extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        fadeAnim: new Animated.Value(0),  // Initial value for opacity: 0
        screenDims: Dimensions.get('window')
    };

    static navigationOptions = ({ navigation, screenProps }) => {
        const params = navigation.state.params || {};
        return {
            title: 'SampleChartsDemo',
            headerBackTitle: 'Back',
            headerStyle: {
                backgroundColor: Config.primaryColor,
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'normal',
                fontSize: 18,
                fontFamily: Config.OBRFont.semiBold
            },
            headerLeft:(<HeaderBackButton title={'Back'} tintColor={'#fff'} onPress={()=>{navigation.navigate('HomeScreen')}}/>),
            headerRight: <SharedDrawerConsumer>
                {({ drawerOpened }) => (
                    <Icon
                        onPress = {() => {
                            drawerOpened(true);
                        }}
                        style={{paddingRight: 20}} name='icon_menu' color={'#fff'} />
                )}

            </SharedDrawerConsumer>
        }
    };


    _dimensionsChangeHandler(dims) {
        this.setState({screenDims: dims.screen});
    }

    componentWillMount() {
        Dimensions.addEventListener("change", this._dimensionsChangeHandler.bind(this));
    }


    componentWillUnmount() {
        // Important to stop updating state after unmount
        Dimensions.removeEventListener("change", this._dimensionsChangeHandler.bind(this));
    }


    render() {
        const {width, height} = this.state.screenDims;

        return (
            <View style={{flex: 1, backgroundColor: '#393862'}}>
                <WebView
                    style={{width: width}}
                    source = {{ uri:
                            'https://0f89c372-f4ea-4fdc-9401-c1af9b3027e2.htmlpasta.com' }}
                />
            </View>
        );
    }
}
