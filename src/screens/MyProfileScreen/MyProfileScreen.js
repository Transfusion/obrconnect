import {connect} from "react-redux";
import React, {Component} from "react";
import {View, Image, Text, ScrollView} from "react-native";
import {Config} from "../../utils/config/config";
import images from "../../assets/images";
import {DetailsRow} from "../../components/DetailsRow/DetailsRow";
import moment from "moment";
import {SharedDrawerConsumer} from "../../providers/SharedDrawerContext";
import Icon from "../../assets/tmlmIcons";

const divider = <View style={{marginTop: 15, marginBottom: 15,
    height: 1, borderRadius: 5, width: 250, backgroundColor: '#ddd'}} />;

class _MyProfileScreen extends Component {

    static navigationOptions = ({ navigation, screenProps }) => {
        const params = navigation.state.params || {};
        return {
            title: 'My Profile',
            headerBackTitle: 'Back',
            headerStyle: {
                backgroundColor: Config.primaryColor,
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'normal',
                fontSize: 18,
                fontFamily: Config.OBRFont.semiBold
            },
            headerRight: <SharedDrawerConsumer>
                {({ drawerOpened }) => (
                    <Icon
                        onPress = {() => {
                            drawerOpened(true);
                        }}
                        style={{paddingRight: 20}} name='icon_menu' color={'#fff'} />
                )}

            </SharedDrawerConsumer>
        }
    };

    constructor(props) {
        super(props);

    }

    render() {
        return (
            <ScrollView style={{backgroundColor: '#fff'}}>
                <View style={{height: 30}} />

                {this.props.profileInfo ?
                    <View>
                        <View style={{alignItems: 'center', justifyContent: 'center', alignSelf: 'center',
                            position: 'absolute'/*, top: 40*/}}>
                            <Image style={{borderRadius: 85, height: 170, width: 170,}} source={images.user03}/>
                            <View style={{borderRadius: 100, height: 160, width: 160, borderWidth: 1, borderColor: Config.accentColor,
                                backgroundColor: 'rgba(0,0,0,0)', position: 'absolute'}} />

                        </View>

                        <View style={{alignItems: 'center', paddingTop: 190}}>
                            <Text style={{fontWeight: 'bold', fontSize: 30, padding: 10}}>
                                {this.props.profileInfo.name}
                            </Text>
                            <Text style={{padding: 10}}>
                                Member since: {moment(this.props.profileInfo.member_since).format('YYYY/MM/DD')}
                            </Text>

                            <View style={{alignItems: 'center', width: 250}}>
                                {divider}
                                <DetailsRow left={'Staff ID'} right={this.props.profileInfo.staff_id}/>
                                {divider}
                                <DetailsRow left={'Branch'} right={this.props.profileInfo.branch}/>
                                {divider}
                                <DetailsRow left={'Business Unit'} right={this.props.profileInfo.business_unit}/>
                            </View>
                        </View>

                    </View>
                    : null}
            </ScrollView>
        );
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
        profileInfo: state.authReducer.profileInfo
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...ownProps,
    }
};

export const MyProfileScreen = connect(mapStateToProps, mapDispatchToProps)(_MyProfileScreen);
